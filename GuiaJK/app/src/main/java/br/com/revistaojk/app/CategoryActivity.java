package br.com.revistaojk.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryActivity extends BaseActivity {

    protected ArrayList<String> listDataHeader;
    protected HashMap<String, JSONArray> listDataChild;
    protected ArrayList<String> listHeaderColor;
    public HashMap<Integer, Bitmap> catImages;

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);


        //analytics tracker
        Tracker t = ((AnalyticsApplication) getApplication()).getTracker(AnalyticsApplication.TrackerName.APP_TRACKER);
        t.setScreenName(this.getClass().getSimpleName());
        t.send(new HitBuilders.AppViewBuilder().build());
        //end analytics tracker
        RelativeLayout base = (RelativeLayout)findViewById(R.id.included_layout_base);
        base.setVisibility(View.GONE);

        ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
                ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);
                expListView.requestFocus();
            }
        });
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                try {
                    String selectedCategory = CategoryActivity.this.listDataChild.get(CategoryActivity.this.listDataHeader.get(groupPosition))
                            .getString(childPosition);


                    final Intent intent = new Intent();
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY,selectedCategory);
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_BACK_FROM_CATEGORY_LIST,true);
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_BACK_CATEGORY_TITLE,CategoryActivity.this.listDataHeader.get(groupPosition));
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_BACK_CATEGORY_ICON,CategoryActivity.this.catImages.get(groupPosition));
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_BACK_CATEGORY_BGCOLOR,CategoryActivity.this.listHeaderColor.get(groupPosition));
                    setResult(601,intent);
                    finish();

                } catch (JSONException e) {
                }


                return true;
            }
        });

        Constants.showProgress(this);

        JSONArray advertisesCategories = Neighborhood.getInstance().advertisesCategories;
        if (advertisesCategories != null && advertisesCategories.length() > 0){
            this.reloadCategories();
            return;
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(Neighborhood.getInstance().cookieStore);
        RequestParams params = new RequestParams();

        List<String> adIds = new ArrayList<String>();
        for (int i = 0; i < Neighborhood.getInstance().neighborhoodsAdvertises.length(); i++){
            try {
                JSONObject ad = Neighborhood.getInstance().neighborhoodsAdvertises.getJSONObject(i);
                adIds.add(ad.getString("id"));
            }
            catch (JSONException e){

            }
        }
        params.put("ids", TextUtils.join(",", adIds));

        client.get(Constants.URL_CATEGORIES_BY_AD_IDS, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {

            }
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                try {
                    Neighborhood.getInstance().setAdvertisesCategories(response.getJSONArray("categories"));
                    CategoryActivity.this.reloadCategories();
                } catch (JSONException e) {
                    Constants.hideProgress();
                    Constants.showTechnicalProblemsMessage(CategoryActivity.this);
                }

            }


            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Constants.hideProgress();
                Constants.showRequestErrorMessage(CategoryActivity.this);
            }

            @Override
            public void onRetry(int retryNo) {

            }
        });

    }

    protected void reloadCategories(){
        try{
            ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);

            if (CategoryActivity.this.mhandler == null){
                CategoryActivity.this.mhandler = new Handler();
            }

            this.listDataHeader = new ArrayList<String>();
            this.listDataChild = new HashMap<String, JSONArray>();
            this.listHeaderColor = new ArrayList<String>();

            JSONArray categories = Neighborhood.getInstance().advertisesCategories;
            if (categories.length() == 0){

                Constants.showNoAdvertisesForNeighborhoodMessage(this);
                CategoryActivity.this.mhandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(MainActivity.THERE_IS_NO_CATEGORY);
                        finish();
                    }
                }, 2000);
            }
            this.catImages = new HashMap<Integer, Bitmap>();
            Integer posAsInteger;
            String catImageURL;
            for (int x = 0 ; x < categories.length(); x++){
                JSONObject category = categories.getJSONObject(x);
                String categoryName = category.getString("name");
                listDataHeader.add(categoryName);
                listDataChild.put(categoryName, category.getJSONArray("children"));
                listHeaderColor.add(category.getString("bgcolor"));

                catImageURL = "";
                posAsInteger = Integer.valueOf(x);
                if (category.has("icon")){
                    catImageURL = category.getString("icon");
                }

                if (catImageURL != null && !catImageURL.equals("null") && !catImageURL.isEmpty()){;
                    new DownloadImageTask(this.catImages,posAsInteger).execute(catImageURL);
                }
                else{
                    this.catImages.put(posAsInteger, null);
                }
            }


            CategoryActivity.this.mhandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);
                    if (expListView.getExpandableListAdapter() == null) {
                        ExpandableListAdapter listAdapter = new ExpandableListAdapter(CategoryActivity.this);
                        listAdapter.setNewData(CategoryActivity.this.listDataHeader, CategoryActivity.this.listDataChild, CategoryActivity.this.catImages, CategoryActivity.this.listHeaderColor);
                        expListView.setAdapter(listAdapter);
                    } else {
                        ((ExpandableListAdapter) expListView.getExpandableListAdapter()).setNewData(CategoryActivity.this.listDataHeader, CategoryActivity.this.listDataChild, CategoryActivity.this.catImages, CategoryActivity.this.listHeaderColor);
                        ((ExpandableListAdapter) expListView.getExpandableListAdapter()).notifyDataSetChanged();
                    }

                    ViewGroup.LayoutParams params = expListView.getLayoutParams();
                    params.height = CategoryActivity.this.listDataHeader.size() * Constants.convertDpToPixels(52, CategoryActivity.this);
                    expListView.setLayoutParams(params);
                    expListView.requestLayout();

                    Constants.hideProgress();
                }
            }, 2000);




        }
        catch (JSONException e){

        }



    }

}
