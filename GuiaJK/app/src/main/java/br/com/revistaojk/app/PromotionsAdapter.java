package br.com.revistaojk.app;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pierreabreup on 7/11/16.
 */
public class PromotionsAdapter extends RecyclerView.Adapter<PromotionsViewHolder> {
    private JSONArray promotions;
    private Context context;

    public PromotionsAdapter(Context context, JSONArray promotions) {
        this.promotions = promotions;
        this.context = context;
    }

    @Override
    public PromotionsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.promotion_item, viewGroup, false);
        return new PromotionsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PromotionsViewHolder holder, int i) {
        try {
            JSONObject promotion = this.promotions.getJSONObject(i);
            holder.vTitle.setText(promotion.getString("title"));
            holder.vText.setText(promotion.getString("text"));
            holder.vPrice.setText(promotion.getString("price"));

            String imageURL = promotion.getString("image");
            if (imageURL != null){
                holder.vImageView.setVisibility(View.VISIBLE);

                if (imageURL != null && !imageURL.equals("null")){
                    holder.vImageView.setImageResource(R.drawable.loading_image);
                    try{
                        DownloadImageTask dImage = new DownloadImageTask((ImageView)holder.vImageView);
                        dImage.setRoundBorders(true);
                        dImage.execute(imageURL);
                    }
                    catch (Exception e){

                    }
                }

            }
            holder.itemView.setTag(promotion);


            holder.vTitle.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-Bd.otf"));
            holder.vTitle.setTextSize(14);

            holder.vText.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeue.otf"));
            holder.vText.setTextSize(10);

            holder.vPrice.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-Bd.otf"));
            holder.vPrice.setTextSize(12);

            String conditionText = promotion.getString("conditions");
            holder.vBtCondition.setTag(conditionText);
            holder.vBtCondition.setOnClickListener(new ImageButton.OnClickListener() {
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(PromotionsAdapter.this.context);
                    dialog.setContentView(R.layout.promotion_condition);

                    String text = (String)v.getTag();
                    TextView conditions = (TextView)dialog.findViewById(R.id.condition);
                    conditions.setText(text);
                    conditions.setTypeface(Typeface.createFromAsset(PromotionsAdapter.this.context.getAssets(), "fonts/HelveticaNeue.otf"));
                    conditions.setTextSize(14);

                    dialog.show();
                }
            });

        }
        catch (JSONException e){

        }



    }

    @Override
    public int getItemCount() {
        return this.promotions.length();
    }
}
