package br.com.revistaojk.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;

import java.util.ArrayList;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

public class BaseActivity extends ActionBarActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static String INTENT_EXTRA_CURRENT_ACTIVITY = "current_activity";
    public static String INTENT_EXTRA_SHOULD_RELOAD_CONTENT = "reload_content";
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    public static final int REQUEST_RESOLVE_ERROR = 1001;
    private boolean mResolvingError = false;
    private boolean isSearchingAdvertisesByLatlong = false;
    protected Handler mhandler;
    protected Runnable  mRunnable;
    public  boolean menuOpened;
    private boolean isUserPositionSearching;
    private AutoCompleteTextView autoCompleteNeigh;
    private CharSequence wroteNeighForAuto;

    protected Activity topActivity;

    public boolean neighborhoodPickerOpened;
    private View menuLayout = null;
    private View pickerNeighborhoodLayout = null;
    private List<String> addedNeighboordList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.isUserPositionSearching = false;
        this.neighborhoodPickerOpened = false;

        Drawable d=getResources().getDrawable(R.drawable.action_bar);
        getSupportActionBar().setBackgroundDrawable(d);

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);

        this.menuOpened = false;

        ImageButton menuButton = (ImageButton) mCustomView.findViewById(R.id.menuImageButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivity.this.showMenuLayout();
            }
        });

        ImageButton geoButton = (ImageButton) mCustomView.findViewById(R.id.geoImageButton);
        geoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                BaseActivity.this.showUserPosition();
            }
        });

        getSupportActionBar().setCustomView(mCustomView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

    }

    protected void showUserPosition(){
        this.isUserPositionSearching = true;
        Constants.showProgress(this.topActivity);
        this.mhandler=new Handler();
        this.mRunnable=new Runnable() {
            @Override
            public void run() {
                reConnectGoogleApiClient();
            }
        };
        mhandler.postDelayed(mRunnable, 1000);
    }

    protected void searchAdvertisesByCurrentLocation(){
        this.isUserPositionSearching = false;
        if (!this.isSearchingAdvertisesByLatlong){

            this.isSearchingAdvertisesByLatlong = true;
            Constants.showProgress(this.topActivity);

            this.mhandler=new Handler();
            this.mRunnable=new Runnable() {
                @Override
                public void run() {
                 reConnectGoogleApiClient();
                }
            };
            mhandler.postDelayed(mRunnable, 1000);
        }

    }

    public void reConnectGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(BaseActivity.this)
                .addConnectionCallbacks(BaseActivity.this)
                .addOnConnectionFailedListener(BaseActivity.this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            String latlong = String.valueOf(mLastLocation.getLatitude()) + "," + String.valueOf(mLastLocation.getLongitude());
            //String latlong = "-22.938610,-43.366722";

            mGoogleApiClient.disconnect();
            if (this.isUserPositionSearching){
                AsyncHttpClient client = new AsyncHttpClient();
                client.setCookieStore(Neighborhood.getInstance().cookieStore);
                RequestParams params = new RequestParams();
                params.put("l", latlong);
                client.get(Constants.URL_SEARCH_USER_NEIGHBORHOOD, params, new JsonHttpResponseHandler() {
                    public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                        try {
                            String neighborhood = response.getString("msg");
                            Constants.hideProgress();
                            Constants.showMessage(BaseActivity.this, "Você está em " + neighborhood);


                        } catch (JSONException e) {
                            Constants.hideProgress();
                            Constants.showTechnicalProblemsMessage(BaseActivity.this);
                        }

                    }
                    public void onFailure(int statusCode,
                                          org.apache.http.Header[] headers,
                                          java.lang.Throwable throwable,
                                          org.json.JSONObject errorResponse) {
                        Constants.hideProgress();
                        Constants.showRequestErrorMessage(BaseActivity.this);
                    }

                });
            }
            else{
                final Intent intent = new Intent(BaseActivity.this, SearchResultActivity.class);
                intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "search_result");
                intent.putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LATLON, latlong);

                this.isSearchingAdvertisesByLatlong = false;
                startActivityForResult(intent, 500);
                Constants.hideProgress();
            }
        }
        else{
            Constants.showMessage(this, R.string.msg_search_result_search_location_fail);
            Constants.hideProgress();
        }
    }
    public void onConnectionSuspended (int cause){
        showSearchLocationError();
        this.isSearchingAdvertisesByLatlong = false;
        Constants.hideProgress();
    }

    public List<String> getAddedNeighboordList(){
        return this.addedNeighboordList;
    }

    public void onConnectionFailed(ConnectionResult result){
        if (mResolvingError) {
            return;
        } else if (result.hasResolution()) try {
            mResolvingError = true;
            result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
        } catch (IntentSender.SendIntentException e) {
            mGoogleApiClient.connect();
        }
        else {
            showSearchLocationError();
            this.isSearchingAdvertisesByLatlong = false;
            mResolvingError = true;
            Constants.hideProgress();
        }
    }

    protected void showSearchLocationError(){
        Constants.showMessage(this, R.string.msg_search_result_search_location_fail);
    }

    protected void showMenuLayout(){
        try{
            if (this.menuLayout == null){
                LayoutInflater inflator = (LayoutInflater) this
                        .getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);

                this.menuLayout = inflator.inflate(R.layout.menu_layout, null);

                TextView publishT1 = (TextView)this.menuLayout.findViewById(R.id.menu_publish_t1);
                publishT1.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.otf"));
                publishT1.setTextSize(20);

                TextView callT3 = (TextView)this.menuLayout.findViewById(R.id.menu_publish_t3);
                callT3.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:+552133427650"));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        BaseActivity.this.startActivity(callIntent);
                    }
                });

                TextView callT4 = (TextView)this.menuLayout.findViewById(R.id.menu_publish_t5);
                callT4.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:+552137387138"));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        BaseActivity.this.startActivity(callIntent);
                    }
                });

                TextView callT5 = (TextView)this.menuLayout.findViewById(R.id.menu_publish_t7);
                callT5.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:+552125784592"));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        BaseActivity.this.startActivity(callIntent);
                    }
                });

                TextView formHeader = (TextView)this.menuLayout.findViewById(R.id.form_header);
                formHeader.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.otf"));
                formHeader.setTextSize(15);

                TextView formHeaderTitle = (TextView)this.menuLayout.findViewById(R.id.form_header_title);
                formHeaderTitle.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.otf"));
                formHeaderTitle.setTextSize(15);

                EditText nameField = (EditText)this.menuLayout.findViewById(R.id.nameField);
                nameField.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.otf"));
                nameField.setTextSize(13);

                EditText mailField = (EditText)this.menuLayout.findViewById(R.id.mailField);
                mailField.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.otf"));
                mailField.setTextSize(13);

                EditText messageField = (EditText)this.menuLayout.findViewById(R.id.messageField);
                messageField.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeue.otf"));
                messageField.setTextSize(13);

                TextView taquaraCall = (TextView)this.menuLayout.findViewById(R.id.taquara_phone);
                taquaraCall.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:+552133427650"));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        BaseActivity.this.startActivity(callIntent);
                    }
                });

                TextView campoCall = (TextView)this.menuLayout.findViewById(R.id.campo_grande_phone);
                campoCall.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:+552137387138"));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        BaseActivity.this.startActivity(callIntent);
                    }
                });

                TextView vilaCall = (TextView)this.menuLayout.findViewById(R.id.vila_isabel_phone);
                vilaCall.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:+552125784592"));
                        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        BaseActivity.this.startActivity(callIntent);
                    }
                });

                this.menuLayout.findViewById(R.id.close_menu).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BaseActivity.this.hideMenu();
                    }
                });

                this.menuLayout.findViewById(R.id.show_about).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BaseActivity.this.menuLayout.findViewById(R.id.menu_itens).setVisibility(View.GONE);
                        BaseActivity.this.menuLayout.findViewById(R.id.about_text).setVisibility(View.VISIBLE);
                    }
                });

                this.menuLayout.findViewById(R.id.close_about).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BaseActivity.this.menuLayout.findViewById(R.id.about_text).setVisibility(View.GONE);
                        BaseActivity.this.menuLayout.findViewById(R.id.menu_itens).setVisibility(View.VISIBLE);
                    }
                });

                this.menuLayout.findViewById(R.id.show_request_form).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        BaseActivity.this.menuLayout.findViewById(R.id.menu_itens).setVisibility(View.GONE);
                        BaseActivity.this.menuLayout.findViewById(R.id.request_form).setVisibility(View.VISIBLE);
                        LinearLayout formTexts = (LinearLayout)BaseActivity.this.menuLayout.findViewById(R.id.request_form_texts);
                        formTexts.setVisibility(View.VISIBLE);
                    }
                });

                this.menuLayout.findViewById(R.id.close_request_form).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        View viewAccess = BaseActivity.this.getCurrentFocus();
                        InputMethodManager inputMethodManager = (InputMethodManager) BaseActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(viewAccess.getWindowToken(), 0);

                        BaseActivity.this.menuLayout.findViewById(R.id.request_form).setVisibility(View.GONE);
                        BaseActivity.this.menuLayout.findViewById(R.id.menu_itens).setVisibility(View.VISIBLE);
                    }
                });

                EditText nameFieldBind    = (EditText)BaseActivity.this.menuLayout.findViewById(R.id.nameField);
                EditText mailFieldBind    = (EditText)BaseActivity.this.menuLayout.findViewById(R.id.mailField);
                EditText messageFieldBind = (EditText)BaseActivity.this.menuLayout.findViewById(R.id.messageField);


                nameFieldBind.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange (View v,boolean hasFocus){
                        if (hasFocus) {
                            LinearLayout formTexts = (LinearLayout)BaseActivity.this.menuLayout.findViewById(R.id.request_form_texts);
                            formTexts.setVisibility(View.GONE);
                        }
                    }
                });
                mailFieldBind.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            LinearLayout formTexts = (LinearLayout) BaseActivity.this.menuLayout.findViewById(R.id.request_form_texts);
                            formTexts.setVisibility(View.GONE);
                        }
                    }
                });
                messageFieldBind.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            LinearLayout formTexts = (LinearLayout) BaseActivity.this.menuLayout.findViewById(R.id.request_form_texts);
                            formTexts.setVisibility(View.GONE);
                        }
                    }
                });



                this.menuLayout.findViewById(R.id.submit_request).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Constants.showProgress(BaseActivity.this);

                        AsyncHttpClient client = new AsyncHttpClient();
                        client.setCookieStore(Neighborhood.getInstance().cookieStore);
                        RequestParams params = new RequestParams();

                        EditText nameField    = (EditText)BaseActivity.this.menuLayout.findViewById(R.id.nameField);
                        EditText mailField    = (EditText)BaseActivity.this.menuLayout.findViewById(R.id.mailField);
                        EditText messageField = (EditText)BaseActivity.this.menuLayout.findViewById(R.id.messageField);

                        params.put("contact[name]", nameField.getText());
                        params.put("contact[email]", mailField.getText());
                        params.put("contact[message]", messageField.getText());

                        client.post(Constants.URL_SEND_MAIL, params, new JsonHttpResponseHandler() {
                            @Override
                            public void onStart() {

                            }


                            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                                Constants.hideProgress();

                                View viewAccess = BaseActivity.this.getCurrentFocus();
                                InputMethodManager inputMethodManager = (InputMethodManager) BaseActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                inputMethodManager.hideSoftInputFromWindow(viewAccess.getWindowToken(), 0);

                                BaseActivity.this.menuLayout.findViewById(R.id.request_form).setVisibility(View.GONE);
                                BaseActivity.this.menuLayout.findViewById(R.id.menu_itens).setVisibility(View.VISIBLE);

                                Constants.showMessage(BaseActivity.this, R.string.msg_request_advertise_success);
                            }


                            public void onFailure(int statusCode,
                                                  org.apache.http.Header[] headers,
                                                  java.lang.Throwable throwable,
                                                  org.json.JSONObject errorResponse) {
                                Constants.hideProgress();

                                View viewAccess = BaseActivity.this.getCurrentFocus();
                                InputMethodManager inputMethodManager = (InputMethodManager) BaseActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                                inputMethodManager.hideSoftInputFromWindow(viewAccess.getWindowToken(), 0);

                                BaseActivity.this.menuLayout.findViewById(R.id.request_form).setVisibility(View.GONE);
                                BaseActivity.this.menuLayout.findViewById(R.id.menu_itens).setVisibility(View.VISIBLE);
                                Constants.showMessage(BaseActivity.this, R.string.msg_request_advertise_fail);
                            }

                            @Override
                            public void onRetry(int retryNo) {

                            }
                        });
                    }
                });

                this.getWindow().addContentView(menuLayout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            }

            this.menuLayout.startAnimation(fromRightAnimation());
            this.menuLayout.setVisibility(View.VISIBLE);
            this.menuOpened = true;

            this.enableContentViewInteraction(false);
        }
        catch (Exception e){

        }


    }

    public boolean isMenuOpened(){
        return menuOpened;
    }

    protected void enableContentViewInteraction(boolean enable){
        try{
            String currentActivy = getIntent().getStringExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY);

            if (currentActivy != null && currentActivy.equals("home")){
                this.topActivity.findViewById(R.id.slideshow).setEnabled(enable);
                this.topActivity.findViewById(R.id.slideshow).setClickable(enable);

                this.topActivity.findViewById(R.id.slideshow).setEnabled(enable);
                this.topActivity.findViewById(R.id.slideshow).setClickable(enable);

                this.topActivity.findViewById(R.id.searchView).setEnabled(enable);
                this.topActivity.findViewById(R.id.searchView).setClickable(enable);

                this.topActivity.findViewById(R.id.bt_nearby_ad).setEnabled(enable);
                this.topActivity.findViewById(R.id.bt_nearby_ad).setClickable(enable);

                this.topActivity.findViewById(R.id.bt_promotions_ad).setEnabled(enable);
                this.topActivity.findViewById(R.id.bt_promotions_ad).setClickable(enable);

                findViewById(R.id.geoImageButton).setClickable(enable);
                findViewById(R.id.geoImageButton).setEnabled(enable);

                findViewById(R.id.geoImageButton).setClickable(enable);
                findViewById(R.id.geoImageButton).setEnabled(enable);
            }
            else {
                this.topActivity.findViewById(R.id.gridView).setEnabled(enable);
                this.topActivity.findViewById(R.id.gridView).setClickable(enable);

            }
        }
        catch (NullPointerException n){

        }

    }

    public void hideMenu(){
        BaseActivity.this.menuLayout.startAnimation(BaseActivity.this.toRightAnimation());
        BaseActivity.this.menuLayout.setVisibility(View.GONE);

        BaseActivity.this.enableContentViewInteraction(true);

        this.menuOpened = false;
    }

    protected Animation fromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromRight.setDuration(500);
        inFromRight.setInterpolator(new AccelerateInterpolator());
        return inFromRight;
    }

    protected Animation toRightAnimation(){
        Animation toFromRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        toFromRight.setDuration(500);
        toFromRight.setInterpolator(new AccelerateInterpolator());
        return toFromRight;
    }

    protected void showBackButton (){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageButton geoButton = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.geoImageButton);
        geoButton.setVisibility(View.GONE);

    }

    protected void hideBackButton(){
        ImageButton geoButton = (ImageButton) getSupportActionBar().getCustomView().findViewById(R.id.geoImageButton);
        geoButton.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    protected void setTopActivity(Activity top){
        this.topActivity = top;
    }

    protected void onStart() {
        super.onStart();
        this.setNeighborhoodPicker();
    }

    protected void reloadNeighAutoComplete(){
        if (this.mhandler == null){
            this.mhandler=new Handler();
        }

        this.mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                BaseActivity.this.autoCompleteNeigh.showDropDown();
            }
        }, 1000);
    }

    protected void setNeighborhoodPicker(){
        if (BaseActivity.this.pickerNeighborhoodLayout != null){
            return;
        }
        RelativeLayout includedBaseLayout = (RelativeLayout)findViewById(R.id.included_layout_base);
        if (includedBaseLayout == null){
            return;
        }
        Button neighTitle = (Button) includedBaseLayout.findViewById(R.id.select_neighborhoods);
        neighTitle.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-BdIt.otf"));
        neighTitle.setTextSize(12);
        neighTitle.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (BaseActivity.this.pickerNeighborhoodLayout == null){
                    BaseActivity.this.addNeighborhoodPickerElements();
                    BaseActivity.this.topActivity.addContentView(BaseActivity.this.pickerNeighborhoodLayout,new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    BaseActivity.this.pickerNeighborhoodLayout.animate().y(-BaseActivity.this.pickerNeighborhoodLayout.getHeight()).setDuration(1).start();
                }



                Button btSelectneigh = (Button)BaseActivity.this.findViewById(R.id.select_neighborhoods);
                int deltaY = btSelectneigh.getTop() + btSelectneigh.getHeight();

                if (BaseActivity.this.neighborhoodPickerOpened){
                    deltaY = -BaseActivity.this.pickerNeighborhoodLayout.getHeight();
                }

                BaseActivity.this.pickerNeighborhoodLayout.animate().y(deltaY).setDuration(500).start();
                BaseActivity.this.enableContentViewInteraction(BaseActivity.this.neighborhoodPickerOpened);
                BaseActivity.this.neighborhoodPickerOpened = !BaseActivity.this.neighborhoodPickerOpened;
            }
        });


    }

    protected void addNeighborhoodPickerElements(){
        LayoutInflater inflator = (LayoutInflater) BaseActivity.this
                .getSystemService(BaseActivity.this.getApplicationContext().LAYOUT_INFLATER_SERVICE);
        this.pickerNeighborhoodLayout = inflator.inflate(R.layout.picker_neighborhood, null);

        Button searchAdvByNeighbors = (Button)this.pickerNeighborhoodLayout.findViewById(R.id.bt_search_add_by_neighbors);
        searchAdvByNeighbors.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                if (BaseActivity.this.addedNeighboordList.size() == 0){
                    Constants.showMessage(BaseActivity.this, "Selecione pelo menos um bairro");
                    return;
                }


                Neighborhood.getInstance().setExtraNeighborhoodName(TextUtils.join(",", BaseActivity.this.addedNeighboordList));

                BaseActivity.this.pickerNeighborhoodLayout.animate().y(-BaseActivity.this.pickerNeighborhoodLayout.getHeight()).setDuration(500).start();
                BaseActivity.this.neighborhoodPickerOpened = false;
                BaseActivity.this.enableContentViewInteraction(true);

                String currentActivy = getIntent().getStringExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY);
                if (currentActivy != null){
                    if (currentActivy.equals("search_result")){
                        SearchResultActivity searchResult = (SearchResultActivity) BaseActivity.this.topActivity;
                        searchResult.searchAdvertises();
                    }
                    else if (currentActivy.equals("home")){
                        loadConversAndCategories((HomeActivity)BaseActivity.this.topActivity);
                    }
                    else{
                        BaseActivity.this.finish();
                    }
                }
            }
        });


        TagContainerLayout nearNeighborhoods = (TagContainerLayout) this.pickerNeighborhoodLayout.findViewById(R.id.near_neighborhoods);
        List<String> nearList = new ArrayList<String>();
        try {
            for (int x=0; x < Neighborhood.getInstance().nearbyNeighborhoods.length(); x++){
                nearList.add(Neighborhood.getInstance().nearbyNeighborhoods.getString(x));
            }
        }
        catch (JSONException e){

        }
        nearNeighborhoods.setTags(nearList);
        nearNeighborhoods.setTagTypeface(Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
        nearNeighborhoods.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                BaseActivity.this.addedNeighboordList.add(text);

                RecyclerView addedNeighborhoodsView = (RecyclerView)BaseActivity.this.pickerNeighborhoodLayout.findViewById(R.id.added_neighborhood);
                AddedNeighborhoodsAdapter addedNeighAdapter = (AddedNeighborhoodsAdapter)addedNeighborhoodsView.getAdapter();
                addedNeighAdapter.updateList(BaseActivity.this.addedNeighboordList);
                addedNeighAdapter.notifyDataSetChanged();
            }

            @Override
            public void onTagLongClick(final int position, String text) {
                // ...
            }
        });

        ArrayAdapter<String> autoCompleteNeighAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1) {

            private Filter filter;
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    LayoutInflater inflator = (LayoutInflater) BaseActivity.this
                            .getSystemService(BaseActivity.this.getApplicationContext().LAYOUT_INFLATER_SERVICE);
                    convertView = inflator.inflate(R.layout.autocomplete,null);
                }

                TextView venueName = (TextView) convertView
                        .findViewById(R.id.search_item_name);

                final String neighName = this.getItem(position);
                venueName.setText(neighName);
                convertView.setTag(neighName);
                return convertView;

            }

            @Override
            public Filter getFilter() {
                if (filter == null) {
                    filter = new NeighborhoodFilter(this,BaseActivity.this);
                }
                return filter;
            }
        };


        this.autoCompleteNeigh = (AutoCompleteTextView)this.pickerNeighborhoodLayout.findViewById(R.id.neighborhood_autocomplete_textview);
        this.autoCompleteNeigh.setAdapter(autoCompleteNeighAdapter);
        this.autoCompleteNeigh.setEnabled(true);
        this.autoCompleteNeigh.setTypeface(Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
        this.autoCompleteNeigh.setTextSize(11);
        this.autoCompleteNeigh.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            }
        });
        this.autoCompleteNeigh.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ImageView iconSearch = (ImageView) BaseActivity.this.pickerNeighborhoodLayout.findViewById(R.id.icon_search);
                if (s.toString().length() == 0){
                    iconSearch.setVisibility(View.VISIBLE);
                }else{
                    iconSearch.setVisibility(View.GONE);
                }

                if (s.toString().indexOf("{") == -1){
                    BaseActivity.this.wroteNeighForAuto = s;
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        this.autoCompleteNeigh.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {

                String clickedNeighborhood = arg1.getTag().toString();


                InputMethodManager in = (InputMethodManager) BaseActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(BaseActivity.this.getCurrentFocus().getApplicationWindowToken(), 0);

                BaseActivity.this.autoCompleteNeigh.setText("");
                BaseActivity.this.autoCompleteNeigh.clearFocus();
                BaseActivity.this.addedNeighboordList.add(clickedNeighborhood);

                RecyclerView addedNeighborhoodsView = (RecyclerView)BaseActivity.this.pickerNeighborhoodLayout.findViewById(R.id.added_neighborhood);
                AddedNeighborhoodsAdapter addedNeighAdapter = (AddedNeighborhoodsAdapter)addedNeighborhoodsView.getAdapter();
                addedNeighAdapter.updateList(BaseActivity.this.addedNeighboordList);
                addedNeighAdapter.notifyDataSetChanged();

            }
        });

        this.addedNeighboordList = new ArrayList<String>();

        String moreNeighborhoodName = Neighborhood.getInstance().extraNeighborhoodName;
        if (moreNeighborhoodName != null && moreNeighborhoodName.length() > 0){
            String[] moreNeighborhoodNameAry = Neighborhood.getInstance().extraNeighborhoodName.split(",");
            for (int it = 0; it < moreNeighborhoodNameAry.length ; it++){
                this.addedNeighboordList.add(moreNeighborhoodNameAry[it]);
            }
        }
        else{
            this.addedNeighboordList.add(Neighborhood.getInstance().currentNeighborhoodName);
        }

        RecyclerView addedNeighborhoodsView = (RecyclerView)this.pickerNeighborhoodLayout.findViewById(R.id.added_neighborhood);
        LinearLayoutManager managerAddedNeigh = new LinearLayoutManager(this);
        managerAddedNeigh.setOrientation(LinearLayoutManager.HORIZONTAL);
        addedNeighborhoodsView.setLayoutManager(managerAddedNeigh);
        addedNeighborhoodsView.setItemAnimator(new DefaultItemAnimator());
        AddedNeighborhoodsAdapter addedNeighAdapter = new AddedNeighborhoodsAdapter(this,this.addedNeighboordList);
        addedNeighborhoodsView.setAdapter(addedNeighAdapter);
        addedNeighborhoodsView.addItemDecoration(new PhotosHorizontalSpace(15));
    }


    protected void loadConversAndCategories(final HomeActivity homeActivity){
        Constants.showProgress(BaseActivity.this);

        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(Neighborhood.getInstance().cookieStore);
        RequestParams params = new RequestParams();
        String neighborhoodName = Neighborhood.getInstance().currentNeighborhoodName;
        String moreNeighborhoodName = Neighborhood.getInstance().extraNeighborhoodName;
        if (moreNeighborhoodName != null && moreNeighborhoodName.length() > 0){
            neighborhoodName = moreNeighborhoodName;
        }
        params.put("n", neighborhoodName);

        client.get(Constants.URL_NEIGHBORHOOD_COVERS_AND_CATEGORIES, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {

            }


            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                try {
                    Neighborhood.getInstance().setNeighborhoodCovers(response.getJSONArray("covers"));
                    Neighborhood.getInstance().setNeighborhoodCategories(response.getJSONArray("categories"));
                    Neighborhood.getInstance().setNearbyNeighborhoods(response.getJSONArray("nearby_neighborhoods"));

                    homeActivity.reloadContent();

                    Constants.hideProgress();


                } catch (JSONException e) {
                    Constants.hideProgress();
                    Constants.showTechnicalProblemsMessage(BaseActivity.this);
                }

            }


            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Constants.hideProgress();
                Constants.showRequestErrorMessage(BaseActivity.this);
            }

            @Override
            public void onRetry(int retryNo) {

            }
        });
    }

    protected void removeAddedNeighborhood(String name){
        this.addedNeighboordList.remove(name);

        RecyclerView addedNeighborhoodsView = (RecyclerView)BaseActivity.this.pickerNeighborhoodLayout.findViewById(R.id.added_neighborhood);
        AddedNeighborhoodsAdapter addedNeighAdapter = (AddedNeighborhoodsAdapter)addedNeighborhoodsView.getAdapter();
        addedNeighAdapter.updateList(BaseActivity.this.addedNeighboordList);
        addedNeighAdapter.notifyDataSetChanged();
    }

    public void reloadAutoComplete(){

    }
}


