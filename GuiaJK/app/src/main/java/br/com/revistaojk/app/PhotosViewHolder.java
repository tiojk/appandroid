package br.com.revistaojk.app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

public class PhotosViewHolder extends RecyclerView.ViewHolder{
    protected ImageButton vImageView;

    public PhotosViewHolder(View v) {
        super(v);
        vImageView =  (ImageButton) v.findViewById(R.id.photo);
    }
}