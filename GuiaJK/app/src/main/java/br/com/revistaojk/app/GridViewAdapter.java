package br.com.revistaojk.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GridViewAdapter extends BaseAdapter {
    private Context context;
    private JSONArray advertises;
    public HashMap<Integer, Bitmap> adImages;
    private Bitmap loadImageBitmap;
    private ArrayList<JSONObject> advertisesCache;
    private Boolean isFreeAdded;

    public GridViewAdapter(Context _context, JSONArray _advertises) {
        this.context = _context;
        this.advertises = _advertises;
        this.adImages = new HashMap<Integer, Bitmap>();
        this.advertisesCache = new ArrayList<JSONObject>();
        this.loadImageBitmap  = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.loading_image);

        this.preLoadImages();
        this.isFreeAdded = false;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View gridView;

        try {
            JSONObject advertise = (JSONObject)this.advertisesCache.get(position);

            if (advertise.getBoolean("payed")){
                gridView = this.payedAdvertiseView(position,advertise,convertView);
            }
            else{
                gridView = this.freeAdvertiseView(position,advertise,convertView);
            }

            TextView title = (TextView) gridView.findViewById(R.id.advertise_title);
            title.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-Hv.otf"));
            title.setTextSize(13);
            title.setText(advertise.getString("title"));

            TextView address = (TextView) gridView.findViewById(R.id.advertise_address);
            address.setText(advertise.getJSONObject("store").getString("address"));
            address.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-LtIt.otf"));
            address.setTextSize(10);

            TextView phone = (TextView) gridView.findViewById(R.id.advertise_phone);
            phone.setText(advertise.getJSONObject("store").getString("phone_summary"));
            phone.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-Hv.otf"));
            phone.setTextSize(11);

            TextView distance = (TextView) gridView.findViewById(R.id.advertise_distance);
            distance.setText(advertise.getJSONObject("store").getString("distance"));
            distance.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-BdIt.otf"));
            distance.setTextSize(10);


        }
        catch (JSONException e){
            gridView = convertView;
        }

        return gridView;
    }

    public View payedAdvertiseView(int position, JSONObject advertise, View convertView){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView;

        contentView = inflater.inflate(R.layout.advertise_list_payed_item, null);

        try{
            TextView detail = (TextView) contentView.findViewById(R.id.advertise_detail);
            detail.setText(advertise.getString("details"));
            detail.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeue.otf"));
            detail.setTextSize(10);

            ImageView adImave  = (ImageView) contentView.findViewById(R.id.advertise_image);

            Integer posAsInteger = Integer.valueOf(position);

            Bitmap roudendImage = null;
            if (this.adImages.get(posAsInteger) != null){
                roudendImage = this.generateBorders(this.adImages.get(posAsInteger));
                if (roudendImage != null){
                    adImave.setImageBitmap(roudendImage);
                }

            }
            else{
                adImave.setImageResource(R.drawable.no_image);
                Bitmap mbitmap = ((BitmapDrawable) adImave.getDrawable()).getBitmap();
                roudendImage = this.generateBorders(mbitmap);
                if (roudendImage != null){
                    adImave.setImageBitmap(roudendImage);
                }

            }

            String rating = advertise.getString("rating");
            String[] ratingCp = rating.split("\\.");
            String p = ratingCp[0];
            int ratingInteger = Integer.parseInt(ratingCp[0]);
            int ratingDecimal = Integer.parseInt(ratingCp[1]);

            for (int i = 1; i <= 5; i++){
                if (i < ratingInteger){
                    this.setStarImage(contentView, i, R.drawable.icon_star_full);
                }

                if (i == ratingInteger && ratingDecimal > 25){
                    if (ratingDecimal > 74){
                        this.setStarImage(contentView,i,R.drawable.icon_star_full);
                    }
                    else{
                        this.setStarImage(contentView,i,R.drawable.icon_star_half);
                    }
                }

            }

            int starTotalValue = advertise.getInt("rate_count");
            String rateText = " avaliação";
            if (starTotalValue > 0){
                rateText = " avaliações";
            }
            rateText = (String.valueOf(starTotalValue) + rateText);

            TextView startTotal = (TextView)contentView.findViewById(R.id.star_total);
            startTotal.setText(rateText);
            startTotal.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeue.otf"));
            startTotal.setTextSize(9);

            ImageView hasPromotion = (ImageView)contentView.findViewById(R.id.has_promotion);
            if (advertise.getBoolean("has_promotion")){
                hasPromotion.setVisibility(View.VISIBLE);
            }

            ImageView adOpen = (ImageView) contentView.findViewById(R.id.ad_open);
            Boolean isOpened = advertise.getBoolean("isopened");
            JSONArray workHours = advertise.getJSONObject("store").getJSONArray("work_hour");
            if (workHours != null && isOpened) {
                adOpen.setVisibility(View.VISIBLE);

            }


            return contentView;
        }
        catch (JSONException e){
            e.printStackTrace();
            return contentView;
        }
    }

    public View freeAdvertiseView(int position, JSONObject advertise, View convertView){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View contentView;

        contentView = inflater.inflate(R.layout.advertise_list_free_item, null);

        try {
            ImageView adOpen = (ImageView) contentView.findViewById(R.id.ad_open);
            Boolean isOpened = advertise.getBoolean("isopened");
            JSONArray workHours = advertise.getJSONObject("store").getJSONArray("work_hour");
            if (workHours != null && isOpened) {
                adOpen.setVisibility(View.VISIBLE);

            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        return contentView;
    }

    public void setStarImage(View gridView, int position, int image){
        String starImageName = "star_" + position;
        int starImageID = this.context.getResources().getIdentifier(starImageName, "id", this.context.getPackageName());
        ImageView starImage = (ImageView)gridView.findViewById(starImageID);
        starImage.setImageResource(image);
    }




    @Override
    public int getCount() {
        return this.advertises.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void preLoadImages(){
        for (int i = 0; i < this.advertises.length(); i++){
            try{
                JSONObject advertise = (JSONObject)this.advertises.getJSONObject(i);
                this.advertisesCache.add(advertise);
                Integer posAsInteger = Integer.valueOf(i);

                String advertiseImageURL = this.getAdvertiseImage(advertise);

                if (advertiseImageURL != null && !advertiseImageURL.equals("null")){
                    this.adImages.put(posAsInteger,this.loadImageBitmap);
                    new DownloadImageTask(this.adImages,posAsInteger).execute(advertiseImageURL);
                }
                else{
                    this.adImages.put(posAsInteger,null);
                }
            }
            catch (JSONException e){

            }
        }
    }

    public String getAdvertiseImage(JSONObject advertise){
        String imageURL = null;
        try{
            imageURL = advertise.getJSONObject("highlight_image").getString("url");
        }
        catch (JSONException e){
        }

        return imageURL;
    }

    public Bitmap generateBorders(Bitmap mbitmap){
        Bitmap imageRounded = null;
        if (mbitmap != null) {
            imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
            Canvas canvas = new Canvas(imageRounded);
            Paint mpaint = new Paint();
            mpaint.setAntiAlias(true);
            mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
            canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 15, 15, mpaint);// Round Image Corner 100 100 100 100
        }
        return imageRounded;
    }

}