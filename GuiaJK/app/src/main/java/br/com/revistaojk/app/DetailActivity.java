package br.com.revistaojk.app;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DetailActivity extends BaseActivity implements OnMapReadyCallback {
    private int advertiseIndex;
    private int advertiseID;
    private JSONObject advertise;
    private View rateLayout;

    private String advertiseTitle;
    private String advertiseStoreName;
    private int advertiseId;
    private double advertiseLat;
    private double advertiseLon;
    private JSONArray phones;
    private int currentPhoneIndex;
    private boolean isPayed;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private boolean mResolvingError = false;

    private Handler mhandler;
    private int selectedRate = 0;
    protected Dialog rateDialog;

    private boolean isShowingMap = false;
    public int iteratorCount;

    public static String INTENT_EXTRA_ADVERTISE_INDEX = "advertise_index";
    public static String INTENT_EXTRA_ADVERTISE_ID = "advertise_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        this.advertiseIndex = getIntent().getIntExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_INDEX, 0);
        this.advertiseID = getIntent().getIntExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_ID, 0);


        if (this.advertiseID > 0){
            this.advertise =  Neighborhood.getInstance().advertiseLoaded;
            this.showAdvertiseDetail(this.advertise);
        }
        else{
            try {
                this.advertise =  Neighborhood.getInstance().neighborhoodsAdvertises.getJSONObject(this.advertiseIndex);
                this.showAdvertiseDetail(this.advertise);
            }
            catch (JSONException e){

            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    protected void onStart() {
        super.onStart();
        showBackButton();
        this.setTopActivity(this);
    }

    protected void showAdvertiseDetail(JSONObject advertise){
        try{

            this.isPayed = advertise.getBoolean("payed");

            if (this.isPayed){
                String imageURL = null;
                try{
                    imageURL = advertise.getJSONObject("image").getString("url");
                }
                catch (JSONException e){
                }

                View imageArea = this.findViewById(R.id.ad_image_area);
                imageArea.setVisibility(View.VISIBLE);

                ImageView adImave  = (ImageView) findViewById(R.id.image);
                if (imageURL != null && !imageURL.equals("null")){
                    adImave.setImageResource(R.drawable.loading_image);
                    try{
                        new DownloadImageTask((ImageView)
                                findViewById(R.id.image))
                                .execute(advertise.getJSONObject("image").getString("url"));
                    }
                    catch (JSONException e){

                    }
                }
            }

            this.advertiseId = advertise.getInt("id");
            this.advertiseTitle = advertise.getString("title");
            this.advertiseStoreName = advertise.getJSONObject("store").getString("name");
            this.advertiseLat = advertise.getJSONObject("location").getDouble("lat");
            this.advertiseLon = advertise.getJSONObject("location").getDouble("lon");

            Map<String, String> articleParams = new HashMap<String, String>();
            articleParams.put("Page", "Advertise Detail");
            articleParams.put("Advertise_Title", this.advertiseTitle);
            articleParams.put("Neighborhood", Neighborhood.getInstance().currentNeighborhoodName);
            FlurryAgent.logEvent("PageView", articleParams);

            Button zapShare = (Button)findViewById(R.id.bt_share_zap);
            zapShare.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, DetailActivity.this.shareText());
                    sendIntent.setType("text/plain");
                    sendIntent.setPackage("com.whatsapp");
                    startActivity(sendIntent);

                }
            });

            Button mailShare = (Button)findViewById(R.id.bt_share_mail);
            mailShare.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.setType("message/rfc822");
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Revista JK - Anúncio compartilhado com você!");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, DetailActivity.this.shareText());
                    startActivity(sendIntent);

                }
            });

            Button addContact = (Button)findViewById(R.id.bt_add_contact);
            addContact.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    DetailActivity.this.writePhoneContact();
                }
            });

            TextView title = (TextView)findViewById(R.id.title);
            title.setText(this.advertiseTitle);
            title.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-Bd.otf"));
            title.setTextSize(14);

            TextView categories = (TextView)findViewById(R.id.categories);
            categories.setText(advertise.getString("categories_names"));
            categories.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
            categories.setTextSize(12);

            TextView details = (TextView)findViewById(R.id.details);
            details.setText(advertise.getString("details"));
            details.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
            details.setTextSize(13);


            TextView address = (TextView)findViewById(R.id.address);
            address.setText(advertise.getJSONObject("store").getString("address"));
            address.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-LtIt.otf"));
            address.setTextSize(12);

            String distanceText = advertise.getJSONObject("store").getString("distance");
            Boolean isValidDistance = (distanceText != null && !distanceText.isEmpty() && !distanceText.equals("0"));
            if (isValidDistance){
                TextView distance = (TextView)findViewById(R.id.distance);
                distance.setText(advertise.getJSONObject("store").getString("distance"));
                distance.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-HvIt.otf"));
                distance.setTextSize(9);
                distance.setVisibility(View.VISIBLE);
            }




            String rating = advertise.getString("rating");
            String[] ratingCp = rating.split("\\.");
            String p = ratingCp[0];
            int ratingInteger = Integer.parseInt(ratingCp[0]);
            int ratingDecimal = Integer.parseInt(ratingCp[1]);

            for (int i = 1; i <= 5; i++){
                if (i < ratingInteger){
                    this.setStarImage( i, R.drawable.icon_star_full);
                }

                if (i == ratingInteger && ratingDecimal > 25){
                    if (ratingDecimal > 74){
                        this.setStarImage(i,R.drawable.icon_star_full);
                    }
                    else{
                        this.setStarImage(i,R.drawable.icon_star_half);
                    }
                }

            }

            int rateCount = advertise.getInt("rate_count");
            String rateCountText;
            if (rateCount > 1){
                rateCountText = String.valueOf(rateCount)+" avaliação";
            }
            else{
                rateCountText = String.valueOf(rateCount)+" avaliações";
            }

            TextView starTotal = (TextView)findViewById(R.id.star_total);
            starTotal.setText(rateCountText);
            starTotal.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
            starTotal.setTextSize(9);


            this.phones = advertise.getJSONObject("store").getJSONArray("phones");
            for (int x=0; x < this.phones.length(); x++){
                this.iteratorCount = x;
                int phoneItemID = this.getResources().getIdentifier("phone_item_" + x, "id", this.getPackageName());
                View phoneItem = (View)this.findViewById(phoneItemID);
                phoneItem.setVisibility(View.VISIBLE);

                int clickButtonId = this.getResources().getIdentifier("phone_item_text_" + x, "id", this.getPackageName());
                Button clickButton = (Button)this.findViewById(clickButtonId);
                clickButton.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-BdIt.otf"));
                clickButton.setTextSize(15);
                clickButton.setTag(phones.getJSONObject(x).getString("call"));
                clickButton.setText(phones.getJSONObject(x).getString("label"));
                clickButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                            String numberString = (String)v.getTag();
                            Map<String, String> articleParams = new HashMap<String, String>();
                            articleParams.put("Neighborhood", Neighborhood.getInstance().currentNeighborhoodName);
                            articleParams.put("Advertise_Title", DetailActivity.this.advertiseTitle);
                            articleParams.put("Advertise_number", numberString);
                            FlurryAgent.logEvent("CallToAdvertise", articleParams);

                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + numberString));
                            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            DetailActivity.this.startActivity(callIntent);

                    }
                });

                if (x > 0){
                    int lineId = this.getResources().getIdentifier("phone_line_" + x, "id", this.getPackageName());
                    View phoneLine = (View)this.findViewById(lineId);
                    phoneLine.setVisibility(View.VISIBLE);
                }

            }
            this.iteratorCount = 0;


            int weekDay = Constants.currentWeekNumber();

            JSONArray workHours = advertise.getJSONObject("store").getJSONArray("work_hour");
            for (int x = 0; x < workHours.length(); x++){
                String openHourText = workHours.getJSONObject(x).getString("open");
                String closeHourText = workHours.getJSONObject(x).getString("close");
                if (openHourText.length() > 0 && closeHourText.length() > 0){
                    int openID = this.getResources().getIdentifier("week_open_" + x, "id", this.getPackageName());
                    int closeID = this.getResources().getIdentifier("week_close_" + x, "id", this.getPackageName());
                    int weekNameID = this.getResources().getIdentifier("week_name_" + x, "id", this.getPackageName());
                    int weekColumnID = this.getResources().getIdentifier("week_column_" + x, "id", this.getPackageName());


                    View weekColumn = (View)this.findViewById(weekColumnID);
                    weekColumn.setVisibility(View.VISIBLE);
                    if (x == weekDay){
                        Drawable d=getResources().getDrawable(R.drawable.week_colum_selected);
                        weekColumn.setBackground(d);
                    }

                    TextView weekName = (TextView)this.findViewById(weekNameID);
                    weekName.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-Bd.otf"));
                    weekName.setTextSize(9);
                    TextView openHour = (TextView)this.findViewById(openID);
                    openHour.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
                    openHour.setTextSize(9);
                    TextView closeHour = (TextView)this.findViewById(closeID);
                    closeHour.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
                    closeHour.setTextSize(9);

                    openHour.setText(openHourText);
                    closeHour.setText(closeHourText);
                }

            }
            ImageView adOpen = (ImageView)findViewById(R.id.ad_open);
            Boolean isOpened = advertise.getBoolean("isopened");
            if (isOpened){
                adOpen.setVisibility(View.VISIBLE);
            }


            JSONArray promotions = advertise.getJSONArray("promotions");
            if (promotions.length() > 0){
                RecyclerView promotionsView = (RecyclerView)findViewById(R.id.promotions);
                LinearLayoutManager promoManager = new LinearLayoutManager(this);
                promoManager.setOrientation(LinearLayoutManager.VERTICAL);
                promotionsView.setLayoutManager(promoManager);
                promotionsView.setItemAnimator(new DefaultItemAnimator());
                PromotionsAdapter adapter = new PromotionsAdapter(this,promotions);
                promotionsView.setAdapter(adapter);
                promotionsView.addItemDecoration(new PromotionsVerticalSpace(10));

                ViewGroup.LayoutParams params = promotionsView.getLayoutParams();
                params.height = promotions.length() * Constants.convertDpToPixels(100, this);
                promotionsView.setLayoutParams(params);
                promotionsView.requestLayout();

                promotionsView.setVisibility(View.VISIBLE);
            }
            else{
                ImageView titlePromotion = (ImageView)findViewById(R.id.title_promotion);
                titlePromotion.setVisibility(View.GONE);
            }

            if (this.advertiseLat != 0 && this.advertiseLon != 0){
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(this);
            }

            ImageButton traceRoute = (ImageButton)findViewById(R.id.bt_trace_route);
            traceRoute.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {


                    final Dialog dialog = new Dialog(DetailActivity.this);
                    dialog.setContentView(R.layout.trace_route);

                    ImageButton waze = (ImageButton) dialog.findViewById(R.id.waze_icon);
                    waze.setOnClickListener(new ImageButton.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DetailActivity.this.nagivateWithWaze();
                        }
                    });
                    Button wazeText = (Button) dialog.findViewById(R.id.waze_text);
                    wazeText.setOnClickListener(new Button.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DetailActivity.this.nagivateWithWaze();
                        }
                    });

                    ImageButton gmaps = (ImageButton) dialog.findViewById(R.id.gmaps_icon);
                    gmaps.setOnClickListener(new ImageButton.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DetailActivity.this.nagivateWithGmaps();
                        }
                    });

                    Button gmapsText = (Button) dialog.findViewById(R.id.gmaps_text          );
                    gmapsText.setOnClickListener(new Button.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DetailActivity.this.nagivateWithGmaps();
                        }
                    });

                    dialog.show();

                }
            });



            JSONArray photos = advertise.getJSONArray("photos");
            if (photos.length() > 0){
                RecyclerView photosView = (RecyclerView)findViewById(R.id.photos);
                LinearLayoutManager managerPhoto = new LinearLayoutManager(this);
                managerPhoto.setOrientation(LinearLayoutManager.HORIZONTAL);
                photosView.setLayoutManager(managerPhoto);
                photosView.setItemAnimator(new DefaultItemAnimator());
                PhotosAdapter photosAdataper = new PhotosAdapter(this,photos);
                photosView.setAdapter(photosAdataper);
                photosView.addItemDecoration(new PhotosHorizontalSpace(15));

                photosView.setVisibility(View.VISIBLE);
            }
            else{
                ImageView titleGallery = (ImageView)findViewById(R.id.title_galley);
                titleGallery.setVisibility(View.GONE);
            }



            JSONArray comments = advertise.getJSONArray("comments");
            if (comments.length() > 0){
                RecyclerView commentsView = (RecyclerView)findViewById(R.id.comments);
                LinearLayoutManager manager_b = new LinearLayoutManager(this);
                manager_b.setOrientation(LinearLayoutManager.VERTICAL);
                commentsView.setLayoutManager(manager_b);
                commentsView.setItemAnimator(new DefaultItemAnimator());
                CommentsAdapter cAdapter = new CommentsAdapter(this,comments);
                commentsView.setAdapter(cAdapter);
                commentsView.addItemDecoration(new PromotionsVerticalSpace(10));

                ViewGroup.LayoutParams commetParams = commentsView.getLayoutParams();
                commetParams.height = comments.length() * Constants.convertDpToPixels(100, this);
                commentsView.setLayoutParams(commetParams);
                commentsView.requestLayout();

                commentsView.setVisibility(View.VISIBLE);
            }

            ImageButton rateButton = (ImageButton)findViewById(R.id.bt_send_rate);
            rateButton.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    DetailActivity.this.showRateModal();
                }
            });
            ImageButton secondRateButton = (ImageButton)findViewById(R.id.bt_send_rate_blue);
            secondRateButton.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    DetailActivity.this.showRateModal();
                }
            });





        }
        catch (JSONException e) {

        }

    }

    public void nagivateWithWaze(){
        String latlong = Double.toString(this.advertiseLat) + "," + Double.toString(this.advertiseLon);

        try
        {
            String url = "waze://?ll="+latlong+"&navigate=yes";
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
            startActivity( intent );
        }
        catch ( ActivityNotFoundException ex  )
        {
            Intent intent =
                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            startActivity(intent);
        }
    }

    public void setStarImage(int position, int image){
        String starImageName = "star_" + position;
        int starImageID = this.getResources().getIdentifier(starImageName, "id", this.getPackageName());
        ImageView starImage = (ImageView)this.findViewById(starImageID);
        starImage.setImageResource(image);
    }

    protected void showRateModal(){
        this.rateDialog = new Dialog(DetailActivity.this);
        rateDialog.setContentView(R.layout.rate_advertise);
        rateDialog.show();

        ImageButton star1Button = (ImageButton) rateDialog.findViewById(R.id.star_1);
        star1Button.setOnClickListener(this.getStarButtonClickListener());

        ImageButton star2Button = (ImageButton) rateDialog.findViewById(R.id.star_2);
        star2Button.setOnClickListener(this.getStarButtonClickListener());

        ImageButton star3Button = (ImageButton) rateDialog.findViewById(R.id.star_3);
        star3Button.setOnClickListener(this.getStarButtonClickListener());

        ImageButton star4Button = (ImageButton) rateDialog.findViewById(R.id.star_4);
        star4Button.setOnClickListener(this.getStarButtonClickListener());

        ImageButton star5Button = (ImageButton) rateDialog.findViewById(R.id.star_5);
        star5Button.setOnClickListener(this.getStarButtonClickListener());

        EditText fieldName = (EditText)rateDialog.findViewById(R.id.nameField);
        fieldName.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
        fieldName.setTextSize(12);

        EditText fieldComment = (EditText)rateDialog.findViewById(R.id.commentField);
        fieldComment.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
        fieldComment.setTextSize(12);

        ImageButton submit = (ImageButton) rateDialog.findViewById(R.id.send_rate_form);
        submit.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View v) {
                DetailActivity.this.sendRate();
            }
        });
    }

    protected Button.OnClickListener getStarButtonClickListener(){
        return new Button.OnClickListener() {
            public void onClick(View v) {
                int clickedStar = Integer.parseInt(v.getTag().toString());
                DetailActivity.this.selectedRate = clickedStar;


                for (int i = 1; i <= 5; i++){
                    String starImageName = "star_" + i;
                    int starImageID = DetailActivity.this.getResources().getIdentifier(starImageName, "id", DetailActivity.this.getPackageName());
                    ImageView starImage = (ImageView)DetailActivity.this.rateDialog.findViewById(starImageID);

                    if (i <= clickedStar){
                        starImage.setImageResource(R.drawable.icon_start_big);
                    }else{
                        starImage.setImageResource(R.drawable.icon_start_big_empty);
                    }

                }


            }
        };
    }

    protected void sendRate(){
        if (this.selectedRate == 0){
            Constants.showMessage(this,"Informe quantas estrelas é este lugar merece.");
            return;
        }

        EditText fieldName = (EditText)rateDialog.findViewById(R.id.nameField);
        EditText fieldComment = (EditText)rateDialog.findViewById(R.id.commentField);

        Editable nameFilled = fieldName.getText();
        Editable commnetFilled = fieldComment.getText();

        if (nameFilled.length() > 0 && commnetFilled.length() == 0){
            Constants.showMessage(this,"Escreva seu comentário sobre este local.");
            return;
        }

        if (nameFilled.length() == 0 && commnetFilled.length() > 0){
            Constants.showMessage(this,"Informe seu nome.");
            return;
        }

        Constants.showProgress(DetailActivity.this);

        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(Neighborhood.getInstance().cookieStore);
        RequestParams params = new RequestParams();
        params.put("score", this.selectedRate);
        params.put("id", DetailActivity.this.advertiseId);
        params.put("user_name",fieldName.getText());
        params.put("user_comment",fieldComment.getText());

        client.post(Constants.URL_RATE, params, new JsonHttpResponseHandler() {
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                Constants.hideProgress();
                Constants.showMessage(DetailActivity.this, R.string.msg_advertise_rate_sent);
                DetailActivity.this.rateDialog.dismiss();
            }


            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Constants.hideProgress();
                Constants.showMessage(DetailActivity.this, R.string.msg_technical_problems);
                DetailActivity.this.rateDialog.dismiss();

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng currentLatLon = new LatLng(this.advertiseLat, this.advertiseLon);

        mMap = googleMap;
        mMap.addMarker(new MarkerOptions().position(currentLatLon).title(this.advertiseStoreName));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLon, 15));

    }


    public void nagivateWithGmaps(){
        Constants.showProgress(this.topActivity);

        this.mhandler=new Handler();
        this.mRunnable=new Runnable() {
            @Override
            public void run() {
                mGoogleApiClient = new GoogleApiClient.Builder(DetailActivity.this)
                        .addConnectionCallbacks(DetailActivity.this)
                        .addOnConnectionFailedListener(DetailActivity.this)
                        .addApi(LocationServices.API)
                        .build();
                mGoogleApiClient.connect();
            }
        };
        mhandler.postDelayed(mRunnable, 1000);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            String userLatlong = String.valueOf(mLastLocation.getLatitude()) +
                    "," +
                    String.valueOf(mLastLocation.getLongitude());

            String adLatlong = Double.toString(this.advertiseLat) + "," + Double.toString(this.advertiseLon);
            String url = "http://maps.google.com/maps?saddr="+userLatlong+"&daddr="+adLatlong+"&directionsmode=driving";
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
            startActivity( intent );

            Constants.hideProgress();

        }
        else{
            Constants.showMessage(this, R.string.msg_search_result_search_location_fail);
            Constants.hideProgress();
        }
    }
    public void onConnectionSuspended (int cause){
        showSearchLocationError();
        Constants.hideProgress();
    }

    public void onConnectionFailed(ConnectionResult result){
        if (mResolvingError) {
            return;
        } else if (result.hasResolution()) try {
            mResolvingError = true;
            result.startResolutionForResult(this, BaseActivity.REQUEST_RESOLVE_ERROR);
        } catch (IntentSender.SendIntentException e) {
            mGoogleApiClient.connect();
        }
        else {
            showSearchLocationError();
            mResolvingError = true;
            Constants.hideProgress();
        }
    }

    protected void showSearchLocationError(){
        Constants.showMessage(this, R.string.msg_search_result_search_location_fail);
    }


    protected String shareText(){
        String text = this.advertiseTitle + " - ";
        try{
            for (int x=0; x < this.phones.length(); x++){
                String label = this.phones.getJSONObject(x).getString("label");
                text = text + ", " + label;
            }
            String address = advertise.getJSONObject("store").getString("address");

            if (address != null && address.length() > 0){
                text = text + " - " + address;
            }

            text = text + "- enviado atraés do aplicativo O JK. Baixe grátis em https://play.google.com/store/apps/details?id=br.com.revistaojk.app";
        }
        catch (JSONException e){

        }


        return text;
    }

    public void writePhoneContact()
    {
        if (this.phones.length() == 0){
            Constants.showMessage(this,"Sem telefones para adicionar");
            return;
        }
        String DisplayName = this.advertiseTitle;
        String WorkNumber = "";
        String MobileNumber = "";
        String HomeNumber = "";

        String emailID = "";
        String company = "";
        String jobTitle = "";
        try{
            WorkNumber = this.phones.getJSONObject(0).getString("call");
        }
        catch (JSONException e){

        }

        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        contactIntent
                .putExtra(ContactsContract.Intents.Insert.NAME, DisplayName)
                .putExtra(ContactsContract.Intents.Insert.PHONE, WorkNumber);

        startActivityForResult(contactIntent, 1);

    }


}
