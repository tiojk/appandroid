package br.com.revistaojk.app;


import android.content.Context;

import com.loopj.android.http.PersistentCookieStore;

import org.json.JSONArray;
import org.json.JSONObject;

public class Neighborhood {
    private static Neighborhood sharedInstance;

    public JSONArray neighborhoodsList;
    public JSONArray neighborhoodCovers;
    public JSONArray neighborhoodCategories;
    public JSONArray neighborhoodsAdvertises;
    public JSONArray nearbyNeighborhoods;
    public String currentNeighborhoodName;
    public String extraNeighborhoodName;
    public JSONObject advertiseLoaded;
    public JSONArray advertisesCategories;
    public PersistentCookieStore cookieStore;


    public static Neighborhood getInstance(){
        if (sharedInstance == null){
            sharedInstance = new Neighborhood();
        }

        return sharedInstance;
    }

    public void setNeighborhoodsList(JSONArray _neighborhoodsList){
        this.neighborhoodsList = _neighborhoodsList;
    }

    public void setNeighborhoodCovers(JSONArray _neighborhoodsCovers){
        this.neighborhoodCovers = _neighborhoodsCovers;
    }

    public void setNeighborhoodCategories(JSONArray _neighborhoodCategories){
        this.neighborhoodCategories = _neighborhoodCategories;
    }

    public void setNeighborhoodsAdvertises(JSONArray _neighborhoodsAdvertises){
        this.neighborhoodsAdvertises = _neighborhoodsAdvertises;
    }
    public void setNearbyNeighborhoods(JSONArray _nearbyNeighborhoods){
        this.nearbyNeighborhoods = _nearbyNeighborhoods;
    }

    public void setCurrentNeighborhoodName(String neighborhoodName){
        this.currentNeighborhoodName = neighborhoodName;
    }

    public void setExtraNeighborhoodName(String neighborhoodName){
        this.extraNeighborhoodName = neighborhoodName;
    }

    public void setAdvertisesCategories(JSONArray _advertisesCategories){
        this.advertisesCategories = _advertisesCategories;
    }

    public void setCookieStore(Context context){
        this.cookieStore = new PersistentCookieStore(context);
    }

    public void setAdvertiseLoaded(JSONObject advertise){
        this.advertiseLoaded = advertise;
    }
}
