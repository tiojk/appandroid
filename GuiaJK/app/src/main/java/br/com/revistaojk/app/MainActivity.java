package br.com.revistaojk.app;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements
        ConnectionCallbacks, OnConnectionFailedListener{

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Handler mhandler;
    private Runnable  mRunnable;
    private boolean mResolvingError = false;
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private boolean shouldShowNoAdvertiseMessage = true;
    private Spinner neighborhoodSpinner;
    private boolean shouldForceShowSpinner;

    /**
     * The {@link Tracker} used to record screen views.
     */
    private Tracker mTracker;

    public static final int THERE_IS_NO_CATEGORY = 2226;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.shouldForceShowSpinner = false;

        //antigo
        //FlurryAgent.init(this, "SVBKNMSYH8X753P877D3");

        FlurryAgent.init(this,"RT9W6YYN4M9VZNKJ3FG7");



        //[START shared_tracker]
        // Obtain the shared Tracker instance.
        //AnalyticsApplication application = (AnalyticsApplication) getApplication();
        //mTracker = application.getDefaultTracker();
        // [END shared_tracker]

        this.buildGoogleApiClient();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == THERE_IS_NO_CATEGORY) {
            this.shouldForceShowSpinner = true;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError && !this.shouldForceShowSpinner) {

            ImageView searchPostionImage = (ImageView)findViewById(R.id.search_position);
            searchPostionImage.setVisibility(View.VISIBLE);

            this.mhandler=new Handler();
            this.mRunnable=new Runnable() {
                @Override
                public void run() {
                    mGoogleApiClient.connect();
                }
            };
            mhandler.postDelayed(mRunnable, 2 * 1000);
        }

        if (this.shouldForceShowSpinner){
            this.shouldShowNoAdvertiseMessage = false;
            showNeighborhoodSpinner();
        }

        FlurryAgent.onStartSession(this);
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            String latlong = String.valueOf(mLastLocation.getLatitude()) +
                    "," +
                    String.valueOf(mLastLocation.getLongitude());
            this.searchAdvertisesByLatLong(latlong);
        }
        else{
            this.shouldShowNoAdvertiseMessage = false;
            this.searchAdvertisesByLatLong(Constants.UNKNOW_LAT_LONG);
            this.showSearchLocationError();
        }
    }
    public void onConnectionSuspended (int cause){
        this.searchAdvertisesByLatLong(Constants.UNKNOW_LAT_LONG);
        showSearchLocationError();
    }

    public void onConnectionFailed(ConnectionResult result){
        if (mResolvingError) {
            return;
        } else if (result.hasResolution()) try {
            mResolvingError = true;
            result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
        } catch (IntentSender.SendIntentException e) {
            mGoogleApiClient.connect();
        }
        else {
            this.searchAdvertisesByLatLong(Constants.UNKNOW_LAT_LONG);
            showSearchLocationError();
            mResolvingError = true;
        }
    }

    private void showSearchLocationError() {
        Constants.showMessage(this, R.string.msg_main_search_location_fail);
    }

    private void searchAdvertisesByLatLong(String latlong){
        //latlong = "-22.938610,-43.366722";
        Neighborhood.getInstance().setCookieStore(this);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(Neighborhood.getInstance().cookieStore);
        RequestParams params = new RequestParams();
        params.put("l", latlong);


        client.get(Constants.URL_CHECK_FOR_NEIGHBORHOOD, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {

            }


            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                try {
                    Neighborhood.getInstance().setNeighborhoodsList(response.getJSONArray("neighborhoods"));
                    Neighborhood.getInstance().setCurrentNeighborhoodName(response.getString("current_neighborhood"));

                    ImageView searchPostionImage = (ImageView) findViewById(R.id.search_position);
                    searchPostionImage.setVisibility(View.GONE);

                    if (response.getInt("advertises_count") == 0) {
                        showNeighborhoodSpinner();
                    } else {
                        showHome();
                    }
                } catch (JSONException e) {
                    Constants.showTechnicalProblemsMessage(MainActivity.this);
                }

            }

            @Override
            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Constants.showRequestErrorMessage(MainActivity.this);
            }

            @Override
            public void onRetry(int retryNo) {

            }
        });
    }






    public void showNeighborhoodSpinner(){
        if (this.shouldShowNoAdvertiseMessage){
            Constants.showNoAdvertisesForNeighborhoodMessage(this);
        }


        this.neighborhoodSpinner = (Spinner)findViewById(R.id.neighborhood_spinner);
        neighborhoodSpinner.setVisibility(View.VISIBLE);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView)v.findViewById(android.R.id.text1)).setText("");
                    ((TextView)v.findViewById(android.R.id.text1)).setHint(getItem(getCount())); //"Hint to be displayed"
                }

                return v;
            }

            @Override
            public int getCount() {
                return super.getCount()-1; // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        try{
            for (int i = 0; i < Neighborhood.getInstance().neighborhoodsList.length(); i++) {
                adapter.add(Neighborhood.getInstance().neighborhoodsList.getString(i));
            }
            adapter.add("Selecione o bairro");
        }catch (JSONException e){

        }

        neighborhoodSpinner.setAdapter(adapter);
        neighborhoodSpinner.setSelection(adapter.getCount()); //set the hint the default selection so it appears on launch.
        neighborhoodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (pos != adapter.getCount()){
                    String neighborhoodName = (String )parent.getItemAtPosition(pos);
                    Neighborhood.getInstance().setCurrentNeighborhoodName(neighborhoodName);
                    showHome();
                }

            }
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });

    }
    public void showHome(){
        final Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY,"home");
        intent.putExtra(BaseActivity.INTENT_EXTRA_SHOULD_RELOAD_CONTENT,true);
        startActivityForResult(intent, 600);
    }

}
