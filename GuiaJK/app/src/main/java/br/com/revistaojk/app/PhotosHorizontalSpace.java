package br.com.revistaojk.app;


import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class PhotosHorizontalSpace extends RecyclerView.ItemDecoration {

    private final int mRightSpace;

    public PhotosHorizontalSpace(int mRightSpace) {
        this.mRightSpace = mRightSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.right = mRightSpace;
    }
}