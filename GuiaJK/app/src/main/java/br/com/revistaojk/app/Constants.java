package br.com.revistaojk.app;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Calendar;

import br.com.revistaojk.app.BuildConfig;

public class Constants {
    public static final String URL_APP_HOST = BuildConfig.URL_APP_HOST;
    public static final String URL_SEARCH_ADVERTISES = URL_APP_HOST + "/advertises/search";
    public static final String URL_NEIGHBORHOOD_COVERS_AND_CATEGORIES = URL_APP_HOST + "/advertises/covers_and_categories";
    public static final String URL_CHECK_FOR_NEIGHBORHOOD = URL_APP_HOST + "/advertises/check_for_neighborhood";
    public static final String URL_MAP_DIRECTION = URL_APP_HOST + "/advertises/map_direction";
    public static final String URL_SEARCH_USER_NEIGHBORHOOD = URL_APP_HOST + "/advertises/search_user_neighborhood";
    public static final String URL_SEND_MAIL = URL_APP_HOST + "/advertises/register.json";
    public static final String URL_RATE = URL_APP_HOST + "/rate.json";
    public static final String URL_AUTOCOMPLETE_ADVERTISES = URL_APP_HOST + "/advertises/autocomplete_search";
    public static final String URL_CATEGORIES_BY_AD_IDS = URL_APP_HOST + "/advertises/categories_by_adids";
    public static final String URL_AUTOCOMPLETE_NEIGHBORHOOD = URL_APP_HOST + "/advertises/autocomplete_neighborhood";
    public static final String UNKNOW_LAT_LONG = "0,0";

    private static ProgressDialog dialog;

    public static void showMessage(Context context, String text)
    {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(text)
                    .setTitle("Revista O JK");

            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        catch (RuntimeException r){

        }

    }

    public static void showMessage(Context context, int text)
    {
        try{
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(context.getString(text))
                    .setTitle("Revista O JK");

            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        catch (RuntimeException r){

        }

    }

    public static void showTechnicalProblemsMessage(Context context){
        showMessage(context, R.string.msg_technical_problems);
    }

    public static void showRequestErrorMessage(Context context){
        showMessage(context, R.string.msg_request_error);
    }

    public static void showNoAdvertisesForNeighborhoodMessage(Context context){
        try{
            String text = String.format(context.getString(R.string.msg_no_advertises_for_neighborhood), Neighborhood.getInstance().currentNeighborhoodName);

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(text).setTitle("Revista O JK");

            builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        catch (RuntimeException r){

        }
    }

    public static void showProgress(Context context){
        try{
            if (dialog == null){
                dialog = new ProgressDialog(context,R.style.defaultDialog);
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setIndeterminate(true);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        }
        catch (Exception e){

        }

    }

    public static void hideProgress(){
        if (dialog != null){
            dialog.hide();
            dialog.dismiss();
            dialog = null;
        }

    }

    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static int convertDpToPixels(int dp, Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

    public static int currentWeekNumber(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        int weekDay = 7;
        switch (day) {
            case Calendar.SUNDAY:
                weekDay = 6;
                break;
            case Calendar.MONDAY:
                weekDay = 0;
                break;
            case Calendar.TUESDAY:
                weekDay = 1;
                break;
            case Calendar.WEDNESDAY:
                weekDay = 2;
                break;
            case Calendar.THURSDAY:
                weekDay = 3;
                break;
            case Calendar.FRIDAY:
                weekDay = 4;
                break;
            case Calendar.SATURDAY:
                weekDay = 5;
                break;
        }
        return weekDay;
    }

}
