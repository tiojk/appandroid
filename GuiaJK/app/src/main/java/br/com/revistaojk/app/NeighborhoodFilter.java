package br.com.revistaojk.app;

import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by pierreabreup on 8/1/16.
 */
public class NeighborhoodFilter  extends Filter {
    private ArrayAdapter<String> searchAdapter;
    protected String term;
    protected FilterResults result;
    protected BaseActivity baseActivity;
    protected boolean onConnection;

    public NeighborhoodFilter(ArrayAdapter<String> _searchAdapter, BaseActivity _baseActivity){
        this.searchAdapter = _searchAdapter;
        this.baseActivity = _baseActivity;
        this.onConnection = false;
    }

    protected FilterResults performFiltering(CharSequence constraint) {
        Boolean onconnection = this.onConnection;
        this.result = new FilterResults();
        this.term = constraint.toString();

        if (this.term == null || this.term.length() < 5 || onconnection) {
            result.values = new JSONArray();
            result.count = 0;
            return result;
        }
        this.onConnection = true;
        //Constants.showProgress(this.baseActivity);


        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(Neighborhood.getInstance().cookieStore);
        RequestParams params = new RequestParams();

        params.put("n", term);

        client.get(Constants.URL_AUTOCOMPLETE_NEIGHBORHOOD, params, new JsonHttpResponseHandler() {
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONArray response) {
                try {


                    Constants.hideProgress();
                    // if no constraint is given, return the whole list
                    if (response.length() == 0) {
                        NeighborhoodFilter.this.result.values = new JSONArray();
                        NeighborhoodFilter.this.result.count = 0;
                    } else {
                        // iterate over the list of venues and find if the venue matches the constraint. if it does, add to the result list
                        final ArrayList<String> retList = new ArrayList<String>();
                        for (int x=0; x < response.length(); x++){
                            retList.add(response.getString(x));
                        }

                        NeighborhoodFilter.this.result.values = retList;
                        NeighborhoodFilter.this.result.count = retList.size();


                        if (retList.size() > 0) {
                            NeighborhoodFilter.this.searchAdapter.clear();
                            for (String o : (ArrayList<String>) retList) {
                                searchAdapter.add(o);
                            }


                            //NeighborhoodFilter.this.baseActivity.reloadNeighAutoComplete();
                        }
                    }
                    NeighborhoodFilter.this.onConnection = false;


                } catch (JSONException e) {
                    try {
                        NeighborhoodFilter.this.onConnection = false;
                        Constants.hideProgress();
                    }
                    catch (Exception ep){
                        NeighborhoodFilter.this.onConnection = false;
                        Constants.hideProgress();
                    }

                }

            }


            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                NeighborhoodFilter.this.onConnection = false;
                Constants.hideProgress();
            }
        });



        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        // we clear the adapter and then pupulate it with the new results

    }
}
