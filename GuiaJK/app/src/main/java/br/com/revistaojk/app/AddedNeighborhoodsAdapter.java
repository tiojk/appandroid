package br.com.revistaojk.app;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by pierreabreup on 8/1/16.
 */
public class AddedNeighborhoodsAdapter extends RecyclerView.Adapter<AddedNeighborhoodsViewHolder>  {
    private List<String> neighborhoods;
    private BaseActivity baseActivity;

    public AddedNeighborhoodsAdapter(BaseActivity _baseActivity, List<String> _neighborhoods) {
        this.neighborhoods = _neighborhoods;
        this.baseActivity = _baseActivity;
    }

    @Override
    public AddedNeighborhoodsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.added_neighborhood_item, viewGroup, false);
        return new AddedNeighborhoodsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddedNeighborhoodsViewHolder holder, int i) {
        holder.btAddedNeighborhood.setText(this.neighborhoods.get(i));
        holder.btAddedNeighborhood.setTypeface(Typeface.createFromAsset(this.baseActivity.getAssets(), "fonts/HelveticaNeue.otf"));
        holder.btAddedNeighborhood.setTextSize(11);
        holder.contentView.setTag(this.neighborhoods.get(i));
        holder.contentView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AddedNeighborhoodsAdapter.this.baseActivity.removeAddedNeighborhood(v.getTag().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.neighborhoods.size();
    }

    public void updateList(List<String> _neighborhoods){
        this.neighborhoods = _neighborhoods;
    }
}
