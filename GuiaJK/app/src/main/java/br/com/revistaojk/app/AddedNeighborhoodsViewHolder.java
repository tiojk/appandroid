package br.com.revistaojk.app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by pierreabreup on 8/1/16.
 */
public class AddedNeighborhoodsViewHolder extends RecyclerView.ViewHolder {
    protected TextView btAddedNeighborhood;
    protected View contentView;

    public AddedNeighborhoodsViewHolder(View v) {
        super(v);
        btAddedNeighborhood =  (TextView) v.findViewById(R.id.add_neighborhood_text);
        contentView = v;
    }
}
