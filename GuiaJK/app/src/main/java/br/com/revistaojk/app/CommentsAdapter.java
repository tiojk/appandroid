package br.com.revistaojk.app;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pierreabreup on 7/11/16.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsViewHolder> {
    private JSONArray comments;
    private Context context;

    public CommentsAdapter(Context context, JSONArray comments) {
        this.comments = comments;
        this.context = context;
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comment_item, viewGroup, false);
        return new CommentsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentsViewHolder holder, int i) {
        try {
            JSONObject comment = this.comments.getJSONObject(i);
            holder.vUser.setText(comment.getString("user"));
            holder.vDate.setText(comment.getString("created"));
            holder.vText.setText(comment.getString("text"));


            holder.itemView.setTag(comment);


            holder.vUser.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeueLTStd-Md.otf"));
            holder.vUser.setTextSize(14);

            holder.vDate.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeue.otf"));
            holder.vDate.setTextSize(12);

            holder.vText.setTypeface(Typeface.createFromAsset(this.context.getAssets(), "fonts/HelveticaNeue.otf"));
            holder.vText.setTextSize(12);

            int ratingInteger = comment.getInt("rating");
            for (int x = 1; x <= 5; x++){
                if (x <= ratingInteger){
                    this.setStarImage(holder.vStarsArea, x, R.drawable.icon_star_full);
                }

            }

        }
        catch (JSONException e){

        }



    }

    @Override
    public int getItemCount() {
        return this.comments.length();
    }

    public void setStarImage(View starsArea, int position, int image){
        String starImageName = "star_" + position;
        int starImageID = this.context.getResources().getIdentifier(starImageName, "id", this.context.getPackageName());
        ImageView starImage = (ImageView)starsArea.findViewById(starImageID);
        starImage.setImageResource(image);
    }
}
