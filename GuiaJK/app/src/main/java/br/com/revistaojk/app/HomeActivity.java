package br.com.revistaojk.app;




import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;

import android.widget.Filter;
import android.widget.ImageView;

import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.flurry.android.FlurryAgent;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.marvinlabs.widget.slideshow.SlideShowView;
import com.marvinlabs.widget.slideshow.adapter.RemoteBitmapAdapter;
import com.marvinlabs.widget.slideshow.transition.SlideAndZoomTransitionFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;


public class HomeActivity extends BaseActivity {

    protected ArrayList<String> listDataHeader;
    protected HashMap<String, JSONArray> listDataChild;
    protected ArrayList<String> listHeaderColor;
    public HashMap<Integer, Bitmap> catImages;
    protected CharSequence wroteSearchText;
    public AutoCompleteTextView textView;

    public void reloadAutoComplete(){
        HomeActivity.this.mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                HomeActivity.this.textView.showDropDown();
            }
        }, 1000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getIntent().putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "home");

        ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
                ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);
                expListView.requestFocus();
            }
        });
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                try {
                    String selectedCategory = HomeActivity.this.listDataChild.get(HomeActivity.this.listDataHeader.get(groupPosition))
                            .getString(childPosition);
                    final Intent intent = new Intent(HomeActivity.this, SearchResultActivity.class);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "search_result");
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY, selectedCategory);
                    startActivityForResult(intent, 500);
                } catch (JSONException e) {
                }


                return true;
            }
        });

        ArrayAdapter<JSONObject> searchAdapter = new ArrayAdapter<JSONObject>(this, android.R.layout.simple_list_item_1) {

            private Filter filter;
            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    LayoutInflater inflator = (LayoutInflater) HomeActivity.this
                            .getSystemService(HomeActivity.this.getApplicationContext().LAYOUT_INFLATER_SERVICE);
                    convertView = inflator.inflate(R.layout.autocomplete,null);
                }

                TextView venueName = (TextView) convertView
                        .findViewById(R.id.search_item_name);

                final JSONObject advertise = this.getItem(position);

                try {
                    venueName.setText(advertise.getString("title"));
                    String convertViewTag = advertise.getString("id");
                    convertViewTag = convertViewTag + "-" + advertise.getString("type") + "-" + advertise.getString("title");
                    convertView.setTag(convertViewTag);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return convertView;

            }

            @Override
            public Filter getFilter() {
                if (filter == null) {
                    filter = new AdvertiseFilter(this,HomeActivity.this);
                }
                return filter;
            }
        };

        this.textView =  (AutoCompleteTextView)findViewById(R.id.searchView);
        this.textView.setAdapter(searchAdapter);
        this.textView.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
        this.textView.setTextSize(13);
        this.textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (HomeActivity.this.mhandler == null){
                        HomeActivity.this.mhandler = new Handler();
                    }

                    HomeActivity.this.mhandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.scroll_view).scrollTo(0, 300);
                        }
                    }, 500);
            }
        });
        this.textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ImageView iconSearch = (ImageView) HomeActivity.this.findViewById(R.id.icon_search);
                if (s.toString().length() == 0){
                    iconSearch.setVisibility(View.VISIBLE);
                }else{
                    iconSearch.setVisibility(View.GONE);
                }

                if (s.toString().indexOf("{") == -1){
                    HomeActivity.this.wroteSearchText = s;
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        this.textView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (HomeActivity.this.textView.getText().length() < 4) {
                    Constants.showMessage(HomeActivity.this, R.string.msg_minimun_search_text);
                    }
                    else{
                        String textToSearch = HomeActivity.this.textView.getText().toString();
                        final Intent intent = new Intent(HomeActivity.this, SearchResultActivity.class);
                        intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "search_result");
                        intent.putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_TEXT, textToSearch);
                        startActivityForResult(intent, 500);
                        return false;
                    }
                }
                return false;
            }
        });
        this.textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {

                Constants.showProgress(HomeActivity.this);

                AsyncHttpClient client = new AsyncHttpClient();
                client.setCookieStore(Neighborhood.getInstance().cookieStore);
                RequestParams params = new RequestParams();
                String[] itemTag = arg1.getTag().toString().split("-");
                HomeActivity.this.textView.setText("");

                if (itemTag[1].equals("advertise")){
                    params.put("id", itemTag[0]);
                    client.get(Constants.URL_SEARCH_ADVERTISES, params, new JsonHttpResponseHandler() {

                        @Override
                        public void onStart() {

                        }
                        public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                            try {
                                JSONArray advertise = response.getJSONArray("advertises");
                                Neighborhood.getInstance().setAdvertiseLoaded(advertise.getJSONObject(0));
                                final Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                                intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "detail_activity");
                                intent.putExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_ID, 1);
                                Constants.hideProgress();
                                startActivityForResult(intent, 500);


                            } catch (JSONException e) {
                                Constants.hideProgress();
                                Constants.showTechnicalProblemsMessage(HomeActivity.this);
                            }

                        }

                        public void onFailure(int statusCode,
                                              org.apache.http.Header[] headers,
                                              java.lang.Throwable throwable,
                                              org.json.JSONObject errorResponse) {
                            Constants.hideProgress();
                            Constants.showRequestErrorMessage(HomeActivity.this);
                        }
                        @Override
                        public void onRetry(int retryNo) {

                        }
                    });
                }
                else{
                    Constants.hideProgress();

                    final Intent intent = new Intent(HomeActivity.this, SearchResultActivity.class);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "search_result");
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY, itemTag[2]);
                    startActivityForResult(intent, 500);
                }
            }
        });

        ImageView iconSearch = (ImageView)findViewById(R.id.icon_search);
        float density = getResources().getDisplayMetrics().density;
        if (density > 3){
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)iconSearch.getLayoutParams();
            params.setMargins(100, 80, 0, 0);
            iconSearch.setLayoutParams(params);
        }


        SlideShowView slideShowView = (SlideShowView) findViewById(R.id.slideshow);

        slideShowView.setOnSlideShowEventListener(new SlideShowView.OnSlideShowEventListener() {
            @Override
            public void beforeSlideShown(SlideShowView parent, int position) {

            }

            @Override
            public void onSlideShown(SlideShowView parent, int position) {
                JSONArray covers = Neighborhood.getInstance().neighborhoodCovers;
                if (covers != null){
                    for (int x = 0; x < covers.length() ; x++){
                        String paginationBulletName = "slide_pagination_image_" + x;
                        int paginationBulletID = getResources().getIdentifier(paginationBulletName, "id", getPackageName());
                        ImageView paginationBullet = (ImageView)findViewById(paginationBulletID);

                        if (x == position){
                            paginationBullet.setImageResource(R.drawable.icon_slide_selected);
                        }
                        else{
                            paginationBullet.setImageResource(R.drawable.icon_slide_unselected);
                        }
                    }
                }
            }

            @Override
            public void beforeSlideHidden(SlideShowView parent, int position) {

            }

            @Override
            public void onSlideHidden(SlideShowView parent, int position) {

            }
        });

        slideShowView.setOnSlideClickListener(new SlideShowView.OnSlideClickListener() {
            @Override
            public void onItemClick(SlideShowView parent, int position) {
                if (Neighborhood.getInstance().neighborhoodCovers.length() > 0){
                    Neighborhood.getInstance().setNeighborhoodsAdvertises(Neighborhood.getInstance().neighborhoodCovers);
                    final Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "detail_activity");
                    intent.putExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_INDEX, position);
                    Constants.hideProgress();
                    startActivityForResult(intent, 500);
                }
            }
        });

        Button nearby = (Button) findViewById(R.id.bt_nearby_ad);
        nearby.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.searchAdvertisesByCurrentLocation();
            }
        });

        Button promotions = (Button) findViewById(R.id.bt_promotions_ad);
        promotions.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                final Intent intent = new Intent(HomeActivity.this, SearchResultActivity.class);
                intent.putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION, true);
                startActivityForResult(intent, 500);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK && intent != null) {

        }
    }

    protected void reloadContent(){
        this.reloadCovers();
        this.reloadCategories();

        ScrollView scroll = (ScrollView) findViewById(R.id.scroll_view);
        scroll.post(new Runnable() {

            @Override
            public void run() {
                ScrollView scroll = (ScrollView) findViewById(R.id.scroll_view);
                scroll.scrollTo(0, 0);
            }
        });


    }

    protected void reloadCovers(){
        SlideShowView slideShowView = (SlideShowView) findViewById(R.id.slideshow);
        slideShowView.setTransitionFactory(new SlideAndZoomTransitionFactory());

        ArrayList<String> coversURLs = new ArrayList<String>();
        try{
            JSONArray covers = Neighborhood.getInstance().neighborhoodCovers;
            for (int x = 0; x < covers.length() ; x++){
                coversURLs.add(covers.getJSONObject(x).getJSONObject("neighborhood_image").getString("url"));

                String paginationBulletName = "slide_pagination_image_" + x;
                int paginationBulletID = getResources().getIdentifier(paginationBulletName, "id", getPackageName());
                ImageView paginationBullet = (ImageView)findViewById(paginationBulletID);
                paginationBullet.setVisibility(View.VISIBLE);
            }

        }
        catch (JSONException e){

        }

        try{
            RemoteBitmapAdapter adapter = new RemoteBitmapAdapter(this, coversURLs);
            slideShowView.setAdapter(adapter);
            slideShowView.play();
        }
        catch (RuntimeException e){

        }


    }

    protected void reloadCategories(){
        try{
            ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);

            if (HomeActivity.this.mhandler == null){
                HomeActivity.this.mhandler = new Handler();
            }

            this.listDataHeader = new ArrayList<String>();
            this.listDataChild = new HashMap<String, JSONArray>();
            this.listHeaderColor = new ArrayList<String>();

            JSONArray categories = Neighborhood.getInstance().neighborhoodCategories;
            if (categories.length() == 0){

                Constants.showNoAdvertisesForNeighborhoodMessage(this);
                HomeActivity.this.mhandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(MainActivity.THERE_IS_NO_CATEGORY);
                        finish();
                    }
                }, 2000);
            }
            this.catImages = new HashMap<Integer, Bitmap>();
            Integer posAsInteger;
            String catImageURL;
            for (int x = 0 ; x < categories.length(); x++){
                JSONObject category = categories.getJSONObject(x);
                String categoryName = category.getString("name");
                listDataHeader.add(categoryName);
                listDataChild.put(categoryName, category.getJSONArray("children"));
                listHeaderColor.add(category.getString("bgcolor"));

                catImageURL = "";
                posAsInteger = Integer.valueOf(x);
                if (category.has("icon")){
                    catImageURL = category.getString("icon");
                }

                if (catImageURL != null && !catImageURL.equals("null") && !catImageURL.isEmpty()){;
                    new DownloadImageTask(this.catImages,posAsInteger).execute(catImageURL);
                }
                else{
                    this.catImages.put(posAsInteger, null);
                }
            }


            HomeActivity.this.mhandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ExpandableListView expListView = (ExpandableListView) findViewById(R.id.categories_list);
                    if (expListView.getExpandableListAdapter() == null) {
                        ExpandableListAdapter listAdapter = new ExpandableListAdapter(HomeActivity.this);
                        listAdapter.setNewData(HomeActivity.this.listDataHeader, HomeActivity.this.listDataChild, HomeActivity.this.catImages, HomeActivity.this.listHeaderColor);
                        expListView.setAdapter(listAdapter);
                    } else {
                        ((ExpandableListAdapter) expListView.getExpandableListAdapter()).setNewData(HomeActivity.this.listDataHeader, HomeActivity.this.listDataChild, HomeActivity.this.catImages, HomeActivity.this.listHeaderColor);
                        ((ExpandableListAdapter) expListView.getExpandableListAdapter()).notifyDataSetChanged();
                    }

                    ViewGroup.LayoutParams params = expListView.getLayoutParams();
                    params.height = HomeActivity.this.listDataHeader.size() * Constants.convertDpToPixels(52, HomeActivity.this);
                    expListView.setLayoutParams(params);
                    expListView.requestLayout();
                }
            }, 2000);




        }
        catch (JSONException e){

        }



    }


    @Override
    protected void onStart() {
        try{
            super.onStart();
            hideBackButton();

            this.setTopActivity(this);

            super.loadConversAndCategories(this);

            Map<String, String> articleParams = new HashMap<String, String>();
            articleParams.put("Page", "Home");
            articleParams.put("Neighborhood", Neighborhood.getInstance().currentNeighborhoodName);
            FlurryAgent.logEvent("PageView", articleParams);
        }
        catch (Exception e){

        }
    }

    public void onBackPressed()
    {
        if (isMenuOpened()){
            hideMenu();
        }
        else{
            super.onBackPressed();
        }
    }

}
