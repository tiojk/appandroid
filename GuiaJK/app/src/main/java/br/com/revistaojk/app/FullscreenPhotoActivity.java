package br.com.revistaojk.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;

public class FullscreenPhotoActivity extends AppCompatActivity {

    private FullscreenPhotoPagerAdapter adapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_photo);

        viewPager = (ViewPager) findViewById(R.id.pager);

        Intent i = getIntent();
        int position = i.getIntExtra("position", 0);
        String photos = i.getStringExtra("photos");

        try {
            JSONArray photosAsArray = new JSONArray(photos);
            adapter = new FullscreenPhotoPagerAdapter(FullscreenPhotoActivity.this,
                    photosAsArray);
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(9);
            // displaying selected image first
            viewPager.setCurrentItem(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
