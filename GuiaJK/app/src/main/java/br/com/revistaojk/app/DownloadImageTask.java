package br.com.revistaojk.app;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by pierreabreup on 9/6/15.
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    HashMap<Integer, Bitmap> asyncImages;
    Integer key;
    int mode;
    private Boolean roundBorders;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
        this.roundBorders = false;
        this.mode = 0;
    }

    public DownloadImageTask(HashMap<Integer, Bitmap> _asyncImages, Integer key){
        this.asyncImages = _asyncImages;
        this.roundBorders = false;
        this.mode = 1;
        this.key = key;
    }

    public void setRoundBorders(Boolean should){
        this.roundBorders = should;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        if (this.mode == 1){
            this.asyncImages.put(this.key,result);
        }
        else{
            Bitmap roudendImage = this.generateBorders(result);
            if (roudendImage != null){
                bmImage.setImageBitmap(roudendImage);
            }

        }

    }

    public Bitmap generateBorders(Bitmap mbitmap){
        Bitmap imageRounded = null;
        if (mbitmap != null){
            imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
            Canvas canvas = new Canvas(imageRounded);
            Paint mpaint = new Paint();
            mpaint.setAntiAlias(true);
            mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
            canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 15, 15, mpaint);// Round Image Corner 100 100 100 100
        }

        return imageRounded;
    }
}
