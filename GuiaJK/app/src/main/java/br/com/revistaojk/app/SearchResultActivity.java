package br.com.revistaojk.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SearchResultActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener{

    public static String INTENT_EXTRA_SELECTED_CATEGORY = "selected_category";
    public static String INTENT_EXTRA_SEARCH_BY_TEXT = "search_by_text";
    public static String INTENT_EXTRA_SEARCH_BY_LATLON = "search_by_latlon";
    public static String INTENT_EXTRA_SEARCH_BY_DELIVERY = "search_by_delivery";
    public static String INTENT_EXTRA_SEARCH_BY_SORT = "search_by_sort";
    public static String INTENT_EXTRA_SEARCH_BY_DISTANCE = "search_by_distance";
    public static String INTENT_EXTRA_SEARCH_BY_PROMOTION = "search_by_promtion";
    public static String INTENT_EXTRA_SEARCH_BY_OPEN = "search_by_open";
    public static String INTENT_EXTRA_SEARCH_BY_LONG_DISTANCE = "search_by_long_distance";
    public static String INTENT_EXTRA_SHOULD_SHOW_MAP = "search_show_map";
    public static String INTENT_EXTRA_BACK_FROM_CATEGORY_LIST = "back_from_category_list";
    public static String INTENT_EXTRA_BACK_CATEGORY_TITLE = "back_category_title";
    public static String INTENT_EXTRA_BACK_CATEGORY_ICON = "back_category_icon";
    public static String INTENT_EXTRA_BACK_CATEGORY_BGCOLOR = "back_category_bgcolor";

    private String latLonForSearch;
    private GoogleMap mMap;
    private Map<Integer, HashMap<String, Object>> markersExtraInfo;
    private Map<String,String> selectedFilters;
    private JSONObject touchedAdOnMap;
    private int touchedAdOnMapIndex;

    protected Dialog filterDialog;

    private Handler mhandler;
    public AutoCompleteTextView textView;
    private boolean isDialogOpen;

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == 601 && intent != null) {
            String selectedCategory = intent.getStringExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY);
            this.getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY, selectedCategory);
            this.getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_BACK_FROM_CATEGORY_LIST, true);

            Button btCategory = (Button)findViewById(R.id.btCelectCategory);
            btCategory.setVisibility(View.GONE);

            TextView textCategory = (TextView)findViewById(R.id.text_category);
            textCategory.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeueLTStd-BlkIt.otf"));
            textCategory.setTextSize(16);
            textCategory.setText(intent.getStringExtra(SearchResultActivity.INTENT_EXTRA_BACK_CATEGORY_TITLE));

            Bitmap icon = (Bitmap) intent.getParcelableExtra(SearchResultActivity.INTENT_EXTRA_BACK_CATEGORY_ICON);
            if (icon != null){
                ImageView categoryIcon = (ImageView) findViewById(R.id.icon_category);
                categoryIcon.setImageBitmap(icon);
            }

            RelativeLayout categoryArea = (RelativeLayout)findViewById(R.id.category_area);
            categoryArea.setVisibility(View.VISIBLE);

            RelativeLayout selectedCategoryArea = (RelativeLayout)findViewById(R.id.selected_categor_area);
            selectedCategoryArea.setVisibility(View.VISIBLE);
            selectedCategoryArea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent = new Intent(SearchResultActivity.this, CategoryActivity.class);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "category_activity");

                    startActivityForResult(intent, 601);
                }
            });

            String bgColor = intent.getStringExtra(SearchResultActivity.INTENT_EXTRA_BACK_CATEGORY_BGCOLOR);
            if (bgColor != null){
                selectedCategoryArea.setBackgroundColor(Color.parseColor(bgColor));
            }


        }
    }

    public void reloadAutoComplete(){
        if (this.mhandler == null){
            return;
        }
        this.mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SearchResultActivity.this.textView.showDropDown();
            }
        }, 1000);
    }

    public void clearAutocomplete(){
        SearchResultActivity.this.mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SearchResultActivity.this.textView.clearFocus();
            }
        }, 1000);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.selectedFilters = new HashMap <String,String>();
        this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DELIVERY,"false");
        this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE,"0");
        this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT,"");

        if (this.isSearchByLatlon() || this.shouldShowMap()){
            setContentView(R.layout.activity_search_result_nearby);

            if (this.isSearchByLatlon()){
                RelativeLayout categoryArea = (RelativeLayout)findViewById(R.id.category_area);
                categoryArea.setVisibility(View.VISIBLE);
                categoryArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(SearchResultActivity.this, CategoryActivity.class);
                        intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "category_activity");

                        startActivityForResult(intent, 601);
                    }
                });

                Button btCategory = (Button)findViewById(R.id.btCelectCategory);
                btCategory.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(SearchResultActivity.this, CategoryActivity.class);
                        intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "category_activity");

                        startActivityForResult(intent, 601);
                    }
                });

            }
        }
        else{
            setContentView(R.layout.activity_search_result);
            this.isDialogOpen = false;
            ImageButton filterModal = (ImageButton) this.findViewById(R.id.bt_filter);
            filterModal.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    if (SearchResultActivity.this.filterDialog == null){
                        SearchResultActivity.this.filterDialog = new Dialog(SearchResultActivity.this);
                        SearchResultActivity.this.filterDialog.setContentView(R.layout.filter_modal);
                    }

                    SearchResultActivity.this.filterDialog.show();

                    TextView deliveryTitle = (TextView)SearchResultActivity.this.filterDialog.findViewById(R.id.delivery_title);
                    deliveryTitle.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    deliveryTitle.setTextSize(12);

                    TextView distanceTitle = (TextView)SearchResultActivity.this.filterDialog.findViewById(R.id.distance_title);
                    distanceTitle.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    distanceTitle.setTextSize(12);

                    TextView sortTitle = (TextView)SearchResultActivity.this.filterDialog.findViewById(R.id.sort_by);
                    sortTitle.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    sortTitle.setTextSize(12);

                    String deliveryOnClicked = (String) SearchResultActivity.this.selectedFilters.get(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DELIVERY);
                    RadioButton deliveryOn = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.delivery_on);
                    deliveryOn.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    deliveryOn.setTextSize(10);
                    deliveryOn.setChecked(deliveryOnClicked.equals("true"));
                    deliveryOn.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DELIVERY,"true");
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.searchAdvertises();

                        }
                    });


                    RadioButton deliveryOff = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.delivery_off);
                    deliveryOff.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    deliveryOff.setTextSize(10);
                    deliveryOff.setChecked(!deliveryOnClicked.equals("true"));
                    deliveryOff.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DELIVERY,"false");
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.searchAdvertises();

                        }
                    });

                    String distanceClicked = (String) SearchResultActivity.this.selectedFilters.get(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE);
                    RadioButton distance_a = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.distance_5);
                    distance_a.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    distance_a.setTextSize(10);
                    distance_a.setChecked(distanceClicked.equals("5"));
                    distance_a.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE,"5");
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.searchAdvertises();

                        }
                    });

                    RadioButton distance_b = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.distance_10);
                    distance_b.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    distance_b.setTextSize(10);
                    distance_b.setChecked(distanceClicked.equals("10"));
                    distance_b.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE,"10");
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.searchAdvertises();

                        }
                    });

                    RadioButton distance_c = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.distance_15);
                    distance_c.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    distance_c.setTextSize(10);
                    distance_c.setChecked(distanceClicked.equals("15"));
                    distance_c.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE,"15");
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                    RadioButton distance_d = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.distance_any);
                    distance_d.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    distance_d.setTextSize(10);
                    distance_d.setChecked(distanceClicked.equals("0") || distanceClicked.equals(""));
                    distance_d.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE,"0");
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                    String sortClicked = (String) SearchResultActivity.this.selectedFilters.get(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT);

                    RadioButton sort_a = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.sort_name_asc);
                    sort_a.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    sort_a.setTextSize(10);
                    sort_a.setChecked(sortClicked.equals("title asc"));
                    sort_a.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT,"title asc");
                            RadioGroup rg2 = (RadioGroup) SearchResultActivity.this.filterDialog.findViewById(R.id.radio_group_2);
                            rg2.clearCheck();
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                    RadioButton sort_b = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.sort_name_desc);
                    sort_b.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    sort_b.setTextSize(10);
                    sort_b.setChecked(sortClicked.equals("title desc"));
                    sort_b.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT,"title desc");
                            RadioGroup rg2 = (RadioGroup) SearchResultActivity.this.filterDialog.findViewById(R.id.radio_group_2);
                            rg2.clearCheck();
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                    RadioButton sort_e = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.sort_near);
                    sort_e.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    sort_e.setTextSize(10);
                    sort_e.setChecked(sortClicked.equals(""));
                    sort_e.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT,"");
                            RadioGroup rg2 = (RadioGroup) SearchResultActivity.this.filterDialog.findViewById(R.id.radio_group_2);
                            rg2.clearCheck();
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                    RadioButton sort_c = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.sort_rate_asc);
                    sort_c.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    sort_c.setTextSize(10);
                    sort_c.setChecked(sortClicked.equals("rating asc"));
                    sort_c.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT,"rating asc");
                            RadioGroup rg1 = (RadioGroup) SearchResultActivity.this.filterDialog.findViewById(R.id.radio_group_1);
                            rg1.clearCheck();
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                    RadioButton sort_d = (RadioButton) SearchResultActivity.this.filterDialog.findViewById(R.id.sort_rate_desc);
                    sort_d.setTypeface(Typeface.createFromAsset(SearchResultActivity.this.getAssets(), "fonts/HelveticaNeue.otf"));
                    sort_d.setTextSize(10);
                    sort_d.setChecked(sortClicked.equals("rating desc"));
                    sort_d.setOnClickListener(new RadioButton.OnClickListener() {
                        public void onClick(View v) {
                            SearchResultActivity.this.isDialogOpen = true;
                            SearchResultActivity.this.selectedFilters.put(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT,"rating desc");
                            RadioGroup rg1 = (RadioGroup) SearchResultActivity.this.filterDialog.findViewById(R.id.radio_group_1);
                            rg1.clearCheck();
                            SearchResultActivity.this.searchAdvertises();
                        }
                    });

                }
            });



            Button btPromo = (Button) this.findViewById(R.id.bt_filter_promo);

            Boolean isPromo = SearchResultActivity.this.getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION,false);
            if (isPromo){
                btPromo.setTextColor(Color.parseColor("#F5F5F5"));
                Drawable d=getResources().getDrawable(R.drawable.border_color_gray_inverted);
                btPromo.setBackground(d);
            }

            btPromo.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue.otf"));
            btPromo.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    Boolean isPromo = SearchResultActivity.this.getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION,false);

                    getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION, !isPromo);
                    SearchResultActivity.this.searchAdvertises();
                    Button btPromo = (Button) SearchResultActivity.this.findViewById(R.id.bt_filter_promo);


                    if (!isPromo){
                        btPromo.setTextColor(Color.parseColor("#F5F5F5"));
                        Drawable d=getResources().getDrawable(R.drawable.border_color_gray_inverted);
                        btPromo.setBackground(d);
                    }
                    else{
                        btPromo.setTextColor(Color.parseColor("#6f6f6e"));
                        Drawable d=getResources().getDrawable(R.drawable.border_color_gray);
                        btPromo.setBackground(d);
                    }
                    SearchResultActivity.this.searchAdvertises();


                }
            });

            Button btOpen = (Button) this.findViewById(R.id.bt_filter_open);
            btOpen.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue.otf"));
            btOpen.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    Boolean isOpen = SearchResultActivity.this.getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_OPEN,false);

                    getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_OPEN, !isOpen);
                    SearchResultActivity.this.searchAdvertises();
                    Button btOpen = (Button) SearchResultActivity.this.findViewById(R.id.bt_filter_open);


                    if (!isOpen){
                        btOpen.setTextColor(Color.parseColor("#F5F5F5"));
                        Drawable d=getResources().getDrawable(R.drawable.border_color_gray_inverted);
                        btOpen.setBackground(d);
                    }
                    else{
                        btOpen.setTextColor(Color.parseColor("#6f6f6e"));
                        Drawable d=getResources().getDrawable(R.drawable.border_color_gray);
                        btOpen.setBackground(d);
                    }
                    SearchResultActivity.this.searchAdvertises();


                }
            });

            Button btFar = (Button) this.findViewById(R.id.bt_filter_faraway);
            btFar.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeue.otf"));
            btFar.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    Boolean isFar = SearchResultActivity.this.getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LONG_DISTANCE,false);

                    getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LONG_DISTANCE, !isFar);
                    SearchResultActivity.this.searchAdvertises();
                    Button btFar = (Button) SearchResultActivity.this.findViewById(R.id.bt_filter_faraway);


                    if (!isFar){
                        btFar.setTextColor(Color.parseColor("#F5F5F5"));
                        Drawable d=getResources().getDrawable(R.drawable.border_color_gray_inverted);
                        btFar.setBackground(d);
                    }
                    else{
                        btFar.setTextColor(Color.parseColor("#6f6f6e"));
                        Drawable d=getResources().getDrawable(R.drawable.border_color_gray);
                        btFar.setBackground(d);
                    }
                    SearchResultActivity.this.searchAdvertises();

                }
            });

            ArrayAdapter<JSONObject> searchAdapter = new ArrayAdapter<JSONObject>(this, android.R.layout.simple_list_item_1) {

                private Filter filter;
                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    if (convertView == null) {
                        LayoutInflater inflator = (LayoutInflater) SearchResultActivity.this
                                .getSystemService(SearchResultActivity.this.getApplicationContext().LAYOUT_INFLATER_SERVICE);
                        convertView = inflator.inflate(R.layout.autocomplete,null);
                    }

                    TextView venueName = (TextView) convertView
                            .findViewById(R.id.search_item_name);

                    final JSONObject advertise = this.getItem(position);

                    try {
                        venueName.setText(advertise.getString("title"));
                        String convertViewTag = advertise.getString("id");
                        convertViewTag = convertViewTag + "-" + advertise.getString("type") + "-" + advertise.getString("title");
                        convertView.setTag(convertViewTag);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return convertView;

                }

                @Override
                public Filter getFilter() {
                    if (filter == null) {
                        filter = new AdvertiseFilter(this,SearchResultActivity.this);
                    }
                    return filter;
                }
            };

            this.textView =  (AutoCompleteTextView)findViewById(R.id.searchView);
            this.textView.setAdapter(searchAdapter);
            this.textView.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
            this.textView.setTextSize(13);

            String searchedText = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_TEXT);
            if (searchedText != null){
                this.textView.setText(searchedText);
            }
            this.textView.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    ImageView iconSearch = (ImageView) SearchResultActivity.this.findViewById(R.id.icon_search);
                    if (s.toString().length() == 0){
                        iconSearch.setVisibility(View.VISIBLE);
                    }else{
                        iconSearch.setVisibility(View.GONE);
                    }

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            this.textView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        if (SearchResultActivity.this.textView.getText().length() < 4) {
                            Constants.showMessage(SearchResultActivity.this, R.string.msg_minimun_search_text);
                        }
                        else{
                            String textToSearch = SearchResultActivity.this.textView.getText().toString();

                            getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_TEXT, textToSearch);

                            SearchResultActivity.this.searchAdvertises();
                            return false;
                        }
                    }
                    return false;
                }
            });
            this.textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                        long id) {

                    Constants.showProgress(SearchResultActivity.this);


                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setCookieStore(Neighborhood.getInstance().cookieStore);
                    RequestParams params = new RequestParams();
                    String[] itemTag = arg1.getTag().toString().split("-");
                    SearchResultActivity.this.textView.setText("");

                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);

                    if (itemTag[1].equals("advertise")){
                        params.put("id", itemTag[0]);
                        client.get(Constants.URL_SEARCH_ADVERTISES, params, new JsonHttpResponseHandler() {

                            @Override
                            public void onStart() {

                            }
                            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                                try {
                                    JSONArray advertise = response.getJSONArray("advertises");
                                    Neighborhood.getInstance().setAdvertiseLoaded(advertise.getJSONObject(0));
                                    final Intent intent = new Intent(SearchResultActivity.this, DetailActivity.class);
                                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "detail_activity");
                                    intent.putExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_ID, 1);
                                    Constants.hideProgress();
                                    startActivityForResult(intent, 500);


                                } catch (JSONException e) {
                                    Constants.hideProgress();
                                    Constants.showTechnicalProblemsMessage(SearchResultActivity.this);
                                }

                            }

                            public void onFailure(int statusCode,
                                                  org.apache.http.Header[] headers,
                                                  java.lang.Throwable throwable,
                                                  org.json.JSONObject errorResponse) {
                                Constants.hideProgress();
                                Constants.showRequestErrorMessage(SearchResultActivity.this);
                            }
                            @Override
                            public void onRetry(int retryNo) {

                            }
                        });
                    }else{
                        Constants.hideProgress();
                        getIntent().putExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY, itemTag[2]);
                        SearchResultActivity.this.searchAdvertises();
                    }
                }
            });

            ImageButton showMap = (ImageButton) findViewById(R.id.bt_map);
            showMap.setOnClickListener(new ImageButton.OnClickListener() {
                public void onClick(View v) {

                    final Intent intent = new Intent(SearchResultActivity.this, SearchResultActivity.class);
                    String selectedCategory = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY);
                    if (selectedCategory != null){
                        intent.putExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY, selectedCategory);
                    }
                    String searchByText = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_TEXT);
                    if (searchByText != null){
                        intent.putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_TEXT, searchByText);
                    }
                    Boolean hasPromotion = SearchResultActivity.this.getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION,false);
                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION, hasPromotion);

                    intent.putExtra(SearchResultActivity.INTENT_EXTRA_SHOULD_SHOW_MAP, true);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "search_result");
                    startActivityForResult(intent, 500);
                    Constants.hideProgress();
                }
            });

        }

    }

    protected Boolean isSearchByLatlon(){
        this.latLonForSearch = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LATLON);
        return (this.latLonForSearch != null);
    }

    protected Boolean shouldShowMap(){
        return this.getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SHOULD_SHOW_MAP,false);
    }


    @Override
    protected void onStart() {
        super.onStart();
        showBackButton();
        Boolean backFromCategory = getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_BACK_FROM_CATEGORY_LIST,false);

        if (this.mMap != null && !backFromCategory) {
            return;
        }
        this.setTopActivity(this);

        this.searchAdvertises();

        if (this.isSearchByLatlon() || this.shouldShowMap()){
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            LinearLayout freeAd = (LinearLayout)findViewById(R.id.included_advertise_free);
            freeAd.setVisibility(View.GONE);
            LinearLayout payedAd = (LinearLayout)findViewById(R.id.included_advertise_payed);
            payedAd.setVisibility(View.GONE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        this.markersExtraInfo = new HashMap<Integer, HashMap<String, Object>>();
        this.mMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        HashMap<String,Object> markerExtra;
        String isPayed;
        for (Map.Entry<Integer, HashMap<String, Object>> itemMakerCp : this.markersExtraInfo.entrySet()) {
            HashMap<String, Object> itemMarkerExtra = (HashMap<String, Object>)itemMakerCp.getValue();
            Marker itemMarker = (Marker)itemMarkerExtra.get("marker");

            if (itemMarkerExtra.get("payed").equals("true")){
                itemMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_red));
            }
            else{
                itemMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_blue));
            }
        }

        LinearLayout freeAd = (LinearLayout)findViewById(R.id.included_advertise_free);
        freeAd.setVisibility(View.GONE);
        LinearLayout payedAd = (LinearLayout)findViewById(R.id.included_advertise_payed);
        payedAd.setVisibility(View.GONE);

        int markerHashCode = marker.hashCode();
        markerExtra = SearchResultActivity.this.markersExtraInfo.get(markerHashCode);
        this.touchedAdOnMapIndex = Integer.parseInt(markerExtra.get("index").toString());
        isPayed = (String)markerExtra.get("payed");
        if (isPayed.equals("true")){
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_red_dark));
            this.showPayedAd(Integer.parseInt(markerExtra.get("index").toString()));
        }
        else{
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin_blue_dark));
            this.showFreeAd(Integer.parseInt(markerExtra.get("index").toString()));
        }

        return true;
    }

    protected void showPayedAd(int index){
        try {
            this.touchedAdOnMap = Neighborhood.getInstance().neighborhoodsAdvertises.getJSONObject(index);
            LinearLayout payedAdLayout = (LinearLayout)findViewById(R.id.included_advertise_payed);
            payedAdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent = new Intent(SearchResultActivity.this, DetailActivity.class);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "detail_activity");
                    intent.putExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_INDEX, SearchResultActivity.this.touchedAdOnMapIndex);

                    startActivityForResult(intent, 502);
                }
            });
            TextView title = (TextView) payedAdLayout.findViewById(R.id.advertise_title);
            title.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-Hv.otf"));
            title.setTextSize(13);
            title.setText(this.touchedAdOnMap.getString("title"));

            TextView address = (TextView) payedAdLayout.findViewById(R.id.advertise_address);
            address.setText(this.touchedAdOnMap.getJSONObject("store").getString("address"));
            address.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-LtIt.otf"));
            address.setTextSize(10);

            TextView phone = (TextView) payedAdLayout.findViewById(R.id.advertise_phone);
            phone.setText(this.touchedAdOnMap.getJSONObject("store").getString("phone_summary"));
            phone.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-Hv.otf"));
            phone.setTextSize(11);

            TextView distance = (TextView) payedAdLayout.findViewById(R.id.advertise_distance);
            distance.setText(this.touchedAdOnMap.getJSONObject("store").getString("distance"));
            distance.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-BdIt.otf"));
            distance.setTextSize(10);

            TextView detail = (TextView) payedAdLayout.findViewById(R.id.advertise_detail);
            detail.setText(this.touchedAdOnMap.getString("details"));
            detail.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
            detail.setTextSize(10);

            String imageURL = null;
            try{
                imageURL = this.touchedAdOnMap.getJSONObject("image").getString("url");
            }
            catch (JSONException e){
            }
            ImageView adImave  = (ImageView) payedAdLayout.findViewById(R.id.advertise_image);
            if (imageURL != null && !imageURL.equals("null")){
                adImave.setImageResource(R.drawable.loading_image);
                try{
                    new DownloadImageTask((ImageView)
                            payedAdLayout.findViewById(R.id.advertise_image))
                            .execute(this.touchedAdOnMap.getJSONObject("image").getString("url"));
                }
                catch (JSONException e){

                }
            }

            String rating = this.touchedAdOnMap.getString("rating");
            String[] ratingCp = rating.split("\\.");
            String p = ratingCp[0];
            int ratingInteger = Integer.parseInt(ratingCp[0]);
            int ratingDecimal = Integer.parseInt(ratingCp[1]);

            for (int i = 1; i <= 5; i++){
                if (i < ratingInteger){
                    this.setStarImage(payedAdLayout, i, R.drawable.icon_star_full);
                }

                if (i == ratingInteger && ratingDecimal > 25){
                    if (ratingDecimal > 74){
                        this.setStarImage(payedAdLayout,i,R.drawable.icon_star_full);
                    }
                    else{
                        this.setStarImage(payedAdLayout,i,R.drawable.icon_star_half);
                    }
                }

            }

            int starTotalValue = this.touchedAdOnMap.getInt("rate_count");
            String rateText = " avaliação";
            if (starTotalValue > 0){
                rateText = " avaliações";
            }
            rateText = (String.valueOf(starTotalValue) + rateText);

            TextView startTotal = (TextView)payedAdLayout.findViewById(R.id.star_total);
            startTotal.setText(rateText);
            startTotal.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeue.otf"));
            startTotal.setTextSize(9);

            ImageView hasPromotion = (ImageView)payedAdLayout.findViewById(R.id.has_promotion);
            if (this.touchedAdOnMap.getBoolean("has_promotion")){
                hasPromotion.setVisibility(View.VISIBLE);
            }

            ImageView adOpen = (ImageView) payedAdLayout.findViewById(R.id.ad_open);
            Boolean isOpened = this.touchedAdOnMap.getBoolean("isopened");
            JSONArray workHours = this.touchedAdOnMap.getJSONObject("store").getJSONArray("work_hour");
            if (workHours != null && isOpened) {
                adOpen.setVisibility(View.VISIBLE);

            }

            payedAdLayout.setVisibility(View.VISIBLE);
        }
        catch (JSONException e){

        }
    }

    protected void showFreeAd(int index){
        try {
            this.touchedAdOnMap = Neighborhood.getInstance().neighborhoodsAdvertises.getJSONObject(index);
            LinearLayout freeAdLayout = (LinearLayout)findViewById(R.id.included_advertise_free);
            freeAdLayout.setVisibility(View.VISIBLE);
            freeAdLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent = new Intent(SearchResultActivity.this, DetailActivity.class);
                    intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "detail_activity");
                    intent.putExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_INDEX, SearchResultActivity.this.touchedAdOnMapIndex);

                    startActivityForResult(intent, 502);
                }
            });

            TextView title = (TextView) freeAdLayout.findViewById(R.id.advertise_title);
            title.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-Hv.otf"));
            title.setTextSize(13);
            title.setText(this.touchedAdOnMap.getString("title"));

            TextView address = (TextView) freeAdLayout.findViewById(R.id.advertise_address);
            address.setText(this.touchedAdOnMap.getJSONObject("store").getString("address"));
            address.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-LtIt.otf"));
            address.setTextSize(10);

            TextView phone = (TextView) freeAdLayout.findViewById(R.id.advertise_phone);
            phone.setText(this.touchedAdOnMap.getJSONObject("store").getString("phone_summary"));
            phone.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-Hv.otf"));
            phone.setTextSize(11);

            TextView distance = (TextView) freeAdLayout.findViewById(R.id.advertise_distance);
            distance.setText(this.touchedAdOnMap.getJSONObject("store").getString("distance"));
            distance.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/HelveticaNeueLTStd-BdIt.otf"));
            distance.setTextSize(10);

            ImageView adOpen = (ImageView) freeAdLayout.findViewById(R.id.ad_open);
            Boolean isOpened = this.touchedAdOnMap.getBoolean("isopened");
            JSONArray workHours = this.touchedAdOnMap.getJSONObject("store").getJSONArray("work_hour");
            if (workHours != null && isOpened) {
                adOpen.setVisibility(View.VISIBLE);

            }
        }
        catch (JSONException e){

        }



    }


    protected void searchAdvertises(){
        Constants.showProgress(SearchResultActivity.this);

        Map<String, String> articleParams = new HashMap<String, String>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(Neighborhood.getInstance().cookieStore);
        RequestParams params = new RequestParams();
        String neighborhoodName = Neighborhood.getInstance().currentNeighborhoodName;
        String moreNeighborhoodName = Neighborhood.getInstance().extraNeighborhoodName;
        if (moreNeighborhoodName != null && moreNeighborhoodName.length() > 0){
            neighborhoodName = moreNeighborhoodName;
        }
        params.put("n", neighborhoodName);

        String selectedCategory = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SELECTED_CATEGORY);
        if (selectedCategory != null){
            params.put("c", selectedCategory);
            articleParams.put("byCategory", selectedCategory);
        }

        String searchByText = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_TEXT);
        if (searchByText != null){
            params.put("t", searchByText);
            articleParams.put("byText", searchByText);
        }

        String isDelivery = (String) SearchResultActivity.this.selectedFilters.get(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DELIVERY);
        if (isDelivery.equals("true")){
            params.put("isdelivery", 1);
        }

        Boolean isPromo = getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_PROMOTION,false);
        if (isPromo){
            params.put("haspromotion", 1);
        }

        Boolean isOpen = getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_OPEN,false);
        if (isOpen){
            params.put("isopened", 1);
        }

        Boolean isLong = getIntent().getBooleanExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LONG_DISTANCE,false);
        if (isLong){
            params.put("islongdistance", 1);
        }

        String sortBY = (String) SearchResultActivity.this.selectedFilters.get(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_SORT);
        if (sortBY != null && sortBY.length() > 0){
            params.put("sort_by", sortBY);
        }


        String distanceClicked = (String) SearchResultActivity.this.selectedFilters.get(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_DISTANCE);
        if (distanceClicked.length() > 0 && !distanceClicked.equals("0")){
            params.put("maxdistance", distanceClicked);
        }



        String searchByLatlon = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LATLON);
        if (searchByLatlon != null){
            params.put("l", searchByLatlon);
            //params.put("l", "-22.938610,-43.366722");
            articleParams.put("byLatLong", Neighborhood.getInstance().currentNeighborhoodName);
        }

        articleParams.put("Page", "Advertises List");
        articleParams.put("Neighborhood", Neighborhood.getInstance().currentNeighborhoodName);
        FlurryAgent.logEvent("PageView", articleParams);

        client.get(Constants.URL_SEARCH_ADVERTISES, params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {

            }


            public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                try {
                    Neighborhood.getInstance().setNeighborhoodsAdvertises(response.getJSONArray("advertises"));
                    Constants.hideProgress();

                    if (SearchResultActivity.this.isDialogOpen){
                        SearchResultActivity.this.isDialogOpen = false;
                        SearchResultActivity.this.filterDialog.dismiss();
                    }

                    if (Neighborhood.getInstance().neighborhoodsAdvertises.length() == 0) {
                        Constants.showMessage(SearchResultActivity.this, R.string.msg_advertises_not_found);
                        GridView gridView = (GridView) SearchResultActivity.this.findViewById(R.id.gridView);
                        gridView.setVisibility(View.INVISIBLE);
                        return;
                    }

                    if (SearchResultActivity.this.isSearchByLatlon() || SearchResultActivity.this.shouldShowMap()) {
                        SearchResultActivity.this.addMapMarkers();
                    }
                    else{
                        reloadContent();
                    }

                } catch (JSONException e) {
                    Constants.hideProgress();
                    Constants.showTechnicalProblemsMessage(SearchResultActivity.this);
                }

            }


            public void onFailure(int statusCode,
                                  org.apache.http.Header[] headers,
                                  java.lang.Throwable throwable,
                                  org.json.JSONObject errorResponse) {
                Constants.hideProgress();
                Constants.showRequestErrorMessage(SearchResultActivity.this);
            }

            @Override
            public void onRetry(int retryNo) {

            }
        });
    }

    protected void addMapMarkers(){
        try{
            this.mMap.clear();

            LatLngBounds.Builder mapBounds = new LatLngBounds.Builder();
            JSONArray advertises = Neighborhood.getInstance().neighborhoodsAdvertises;
            LatLng storeLocation;
            LatLng centerMapLocation = null;
            int centerZoom = 14;

            for (int i = 0; i < advertises.length(); i++){
                JSONObject advertise = advertises.getJSONObject(i);
                storeLocation = new LatLng(advertise.getJSONObject("location").getDouble("lat"),
                        advertise.getJSONObject("location").getDouble("lon"));

                if (storeLocation.latitude != 0){
                    if (centerMapLocation == null){
                        centerMapLocation = storeLocation;
                    }
                    mapBounds.include(storeLocation);
                    MarkerOptions newMarker = new MarkerOptions();
                    newMarker.position(storeLocation);
                    newMarker.title(advertises.getJSONObject(i).getJSONObject("store").getString("name"));

                    if (advertise.getBoolean("payed")){
                        newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_red));
                    }
                    else{
                        newMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_blue));
                    }

                    Marker addedMarke = this.mMap.addMarker(newMarker);
                    HashMap<String, Object> markerExtra = new HashMap<String,Object>();
                    markerExtra.put("index",String.valueOf(i));
                    markerExtra.put("payed",String.valueOf(advertise.getBoolean("payed")));
                    markerExtra.put("marker",addedMarke);

                    this.markersExtraInfo.put(addedMarke.hashCode(),markerExtra);
                }
            }

            String userLocationAsStr = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LATLON);
            if (userLocationAsStr != null){
                String[] userLocationAsAry = getIntent().getStringExtra(SearchResultActivity.INTENT_EXTRA_SEARCH_BY_LATLON).split(",");
                centerMapLocation = new LatLng(Double.parseDouble(userLocationAsAry[0]),Double.parseDouble(userLocationAsAry[1]));
            }
            CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(centerMapLocation,centerZoom);
            this.mMap.animateCamera(cu);
        }
        catch (JSONException e){
            e.printStackTrace();
        }

    }

    protected void reloadContent() {
        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setVisibility(View.VISIBLE);

        JSONArray advertises = Neighborhood.getInstance().neighborhoodsAdvertises;

        gridView.setAdapter(new GridViewAdapter(this, advertises));
        gridView.setVerticalSpacing(20);

        this.mhandler = new Handler();


        for (int x = 1; x < 8; x++) {
            this.mhandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GridView gridView = (GridView) findViewById(R.id.gridView);
                    gridView.invalidateViews();
                }
            }, x * 1000);
        }


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

            final Intent intent = new Intent(SearchResultActivity.this, DetailActivity.class);
            intent.putExtra(BaseActivity.INTENT_EXTRA_CURRENT_ACTIVITY, "detail_activity");
            intent.putExtra(DetailActivity.INTENT_EXTRA_ADVERTISE_INDEX, position);

            startActivityForResult(intent, 502);

            }
        });


        try{
            JSONObject firstAd = advertises.getJSONObject(0);
            if (!firstAd.getBoolean("payed")){
                ImageView highligh = (ImageView)findViewById(R.id.highlight_title);
                highligh.setVisibility(View.GONE);
            }
        }
        catch (JSONException e){
        }


    }

    @Override
    public void onBackPressed()
    {
        finish();
    }

    public void setStarImage(View adView, int position, int image){
        String starImageName = "star_" + position;
        int starImageID = this.getResources().getIdentifier(starImageName, "id", this.getPackageName());
        ImageView starImage = (ImageView)adView.findViewById(starImageID);
        starImage.setImageResource(image);
    }

}
