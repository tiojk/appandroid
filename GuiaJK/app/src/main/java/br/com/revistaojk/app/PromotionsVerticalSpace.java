package br.com.revistaojk.app;


import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class PromotionsVerticalSpace extends RecyclerView.ItemDecoration {

    private final int mVerticalSpaceHeight;

    public PromotionsVerticalSpace(int mVerticalSpaceHeight) {
        this.mVerticalSpaceHeight = mVerticalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = mVerticalSpaceHeight;
    }
}