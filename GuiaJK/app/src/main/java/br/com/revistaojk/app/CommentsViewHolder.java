package br.com.revistaojk.app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentsViewHolder extends RecyclerView.ViewHolder{
    protected ImageView vImageView;
    protected TextView vUser;
    protected TextView vDate;
    protected TextView vText;
    protected View vStarsArea;

    public CommentsViewHolder(View v) {
        super(v);
        vUser =  (TextView) v.findViewById(R.id.comment_title);
        vText = (TextView)  v.findViewById(R.id.comment_text);
        vDate = (TextView)  v.findViewById(R.id.comment_date);
        vStarsArea = (View) v.findViewById(R.id.stars_area);
    }
}