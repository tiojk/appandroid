package br.com.revistaojk.app;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by pierreabreup on 7/17/16.
 */
public class FullscreenPhotoPagerAdapter extends PagerAdapter {
    private Activity _activity;
    private JSONArray photos;
    private LayoutInflater inflater;

    // constructor
    public FullscreenPhotoPagerAdapter(Activity activity,
                                       JSONArray photos) {
        this._activity = activity;
        this.photos = photos
        ;
    }

    @Override
    public int getCount() {
        return this.photos.length();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        TouchImageView imgDisplay;
        ImageButton btnClose;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.fullscreen_photo_image, container,
                false);

        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (ImageButton) viewLayout.findViewById(R.id.btnClose);

        try{
            String imageURL = this.photos.getJSONObject(position).getString("image");
            if (imageURL != null && !imageURL.equals("null")){
                imgDisplay.setImageResource(R.drawable.loading_image);
                try{
                    DownloadImageTask dImage = new DownloadImageTask((TouchImageView)imgDisplay);
                    dImage.setRoundBorders(false);
                    dImage.execute(imageURL);
                }
                catch (Exception e){

                }
            }
        }
        catch (JSONException e){

        }


        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.finish();
            }
        });

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}
