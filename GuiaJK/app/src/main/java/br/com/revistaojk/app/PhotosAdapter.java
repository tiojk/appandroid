package br.com.revistaojk.app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by pierreabreup on 7/11/16.
 */
public class PhotosAdapter extends RecyclerView.Adapter<PhotosViewHolder> {
    private JSONArray photos;
    private Context context;

    public PhotosAdapter(Context context, JSONArray photos) {
        this.photos = photos;
        this.context = context;
    }

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.photo_item, viewGroup, false);
        return new PhotosViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotosViewHolder holder, int i) {
        try {
            JSONObject comment = this.photos.getJSONObject(i);
            String imageURL = comment.getString("image");
            if (imageURL != null && !imageURL.equals("null")){
                holder.vImageView.setImageResource(R.drawable.loading_image);
                holder.vImageView.setTag(Integer.valueOf(i));
                try{
                    DownloadImageTask dImage = new DownloadImageTask((ImageView)holder.vImageView);
                    dImage.setRoundBorders(false);
                    dImage.execute(imageURL);

                    holder.vImageView.setOnClickListener(new ImageButton.OnClickListener() {
                        public void onClick(View v) {
                            Intent photoIntent = new Intent(PhotosAdapter.this.context, FullscreenPhotoActivity.class);
                            int tagValue = (Integer)v.getTag();
                            photoIntent.putExtra("position", tagValue);
                            photoIntent.putExtra("photos",photos.toString());
                            PhotosAdapter.this.context.startActivity(photoIntent);
                        }
                    });
                }
                catch (Exception e){

                }
            }

        }
        catch (JSONException e){

        }



    }

    @Override
    public int getItemCount() {
        return this.photos.length();
    }



}
