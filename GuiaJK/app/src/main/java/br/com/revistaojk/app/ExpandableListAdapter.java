package br.com.revistaojk.app;


import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listHeaderColor; // header titles
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, JSONArray> _listDataChild;
    public HashMap<Integer, Bitmap> catImages;

    public int bgGroupColorIndex;
    private int viewChildHeight;
    public Boolean hasData = false;
    private Handler mhandler;


    public ExpandableListAdapter(Context context) {
        this._context = context;
        this.bgGroupColorIndex = 0;
        this.hasData = true;
        this.viewChildHeight = Constants.convertDpToPixels(37,this._context);
    }

    public void setNewData(List<String> listDataHeader,
                           HashMap<String, JSONArray> listChildData, HashMap<Integer, Bitmap> _catImages,List<String> listHeaderColor){
        this.catImages = _catImages;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._listHeaderColor = listHeaderColor;

        this.mhandler = new Handler();
        for (int x = 1; x <= 8 ; x++){
            this.mhandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ExpandableListAdapter.this.notifyDataSetChanged();
                }
            }, x * 1000);
        }

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        try{
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .getString(childPosititon);
        }
        catch (JSONException e){
            return "";
        }

    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.category_child_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.child_name);
        txtListChild.setTypeface(Typeface.createFromAsset(this._context.getAssets(), "fonts/HelveticaNeueLTStd-It.otf"));
        txtListChild.setTextSize(14);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .length();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.category_parent_item, null);
        }

        convertView.setBackgroundColor(Color.parseColor(this._listHeaderColor.get(groupPosition)));
        convertView.invalidate();

        try {
            TextView lblListHeader = (TextView) convertView.findViewById(R.id.category_name);
            lblListHeader.setText(headerTitle);
            lblListHeader.setTypeface(Typeface.createFromAsset(this._context.getAssets(), "fonts/HelveticaNeueLTStd-BlkIt.otf"));
            lblListHeader.setTextSize(15);
            lblListHeader.setTextColor(Color.WHITE);

            ImageView catImageView  = (ImageView) convertView.findViewById(R.id.category_icon);
            Integer posAsInteger = Integer.valueOf(groupPosition);

            if (this.catImages.get(posAsInteger) != null){
                catImageView.setImageBitmap(this.catImages.get(posAsInteger));
            }
            else{
                catImageView.setImageResource(android.R.color.transparent);
            }

            ImageView collapsedSymbol = (ImageView) convertView.findViewById(R.id.collapsed_symbol);
            if (isExpanded){
                collapsedSymbol.setImageResource(R.drawable.icon_minus);
            }
            else{
                collapsedSymbol.setImageResource(R.drawable.icon_plus);
            }
        }
        catch (Exception e){

        }




        return convertView;
    }

    @Override
    public void onGroupExpanded (int groupPosition){
        ExpandableListView expListView = (ExpandableListView) ((Activity) this._context).findViewById(R.id.categories_list);
        ViewGroup.LayoutParams params = expListView.getLayoutParams();
        params.height = params.height + (getChildrenCount(groupPosition) * this.viewChildHeight);
        expListView.setLayoutParams(params);
    }

    public void onGroupCollapsed (int groupPosition){
        ExpandableListView expListView = (ExpandableListView) ((Activity) this._context).findViewById(R.id.categories_list);
        ViewGroup.LayoutParams params = expListView.getLayoutParams();
        params.height = params.height - (getChildrenCount(groupPosition) * this.viewChildHeight);
        expListView.setLayoutParams(params);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}