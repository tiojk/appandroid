package br.com.revistaojk.app;

import android.support.v7.widget.RecyclerView;
import android.view.View;
//import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class PromotionsViewHolder extends RecyclerView.ViewHolder{
    protected ImageView vImageView;
    protected TextView vTitle;
    protected TextView vText;
    protected TextView vPrice;
    protected ImageButton vBtCondition;

    public PromotionsViewHolder(View v) {
        super(v);
        vImageView = (ImageView) v.findViewById(R.id.promo_image);
        vTitle =  (TextView) v.findViewById(R.id.promo_title);
        vText = (TextView)  v.findViewById(R.id.promo_text);
        vPrice = (TextView)  v.findViewById(R.id.promo_price);
        vBtCondition = (ImageButton) v.findViewById(R.id.bt_show_conditions);

//        Button showConditions = (Button)v.findViewById(R.id.bt_show_conditions);
//        showConditions.setOnClickListener(new Button.OnClickListener() {
//            public void onClick(View v) {
//
//            }
//        });


    }
}