package br.com.revistaojk.app;


import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by pierreabreup on 7/24/16.
 */
public class AdvertiseFilter extends Filter {
    private ArrayAdapter<JSONObject> searchAdapter;
    protected String term;
    protected FilterResults result;
    protected BaseActivity baseActivity;
    protected boolean onConnection;


    public AdvertiseFilter(ArrayAdapter<JSONObject> _searchAdapter, HomeActivity _homeActivity){
        this.searchAdapter = _searchAdapter;
        this.baseActivity = _homeActivity;
        this.onConnection = false;
    }

    public AdvertiseFilter(ArrayAdapter<JSONObject> _searchAdapter, SearchResultActivity _searchResultActivity){
        this.searchAdapter = _searchAdapter;
        this.baseActivity = _searchResultActivity;
        this.onConnection = false;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        Boolean onconnection = this.onConnection;
        this.result = new FilterResults();
        this.term = constraint.toString();

        if (this.term == null || this.term.length() < 5 || onconnection) {
            result.values = new JSONArray();
            result.count = 0;
            return result;
        }
        this.onConnection = true;


        if (this.baseActivity != null){
            this.baseActivity.runOnUiThread(new Runnable() {
                public void run() {
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setCookieStore(Neighborhood.getInstance().cookieStore);
                    RequestParams params = new RequestParams();

                    String neighborhoodName = Neighborhood.getInstance().currentNeighborhoodName;
                    String moreNeighborhoodName = Neighborhood.getInstance().extraNeighborhoodName;
                    if (moreNeighborhoodName != null && moreNeighborhoodName.length() > 0){
                        neighborhoodName = moreNeighborhoodName;
                    }
                    params.put("n", neighborhoodName);
                    params.put("t", AdvertiseFilter.this.term);
                    client.get(Constants.URL_AUTOCOMPLETE_ADVERTISES, params, new JsonHttpResponseHandler() {
                        public void onSuccess(int statusCode, org.apache.http.Header[] headers, JSONObject response) {
                            try {
                                JSONArray list = response.getJSONArray("advertises");
                                JSONArray categories = response.getJSONArray("categories");



                                if (list.length() == 0 && categories.length() == 0) {
                                    AdvertiseFilter.this.result.values = new JSONArray();
                                    AdvertiseFilter.this.result.count = 0;
                                } else {

                                    final ArrayList<JSONObject> retList = new ArrayList<JSONObject>();
                                    for (int x=0; x < categories.length(); x++){
                                        retList.add(categories.getJSONObject(x));
                                    }
                                    for (int x=0; x < list.length(); x++){
                                        retList.add(list.getJSONObject(x));
                                    }

                                    AdvertiseFilter.this.result.values = retList;
                                    AdvertiseFilter.this.result.count = retList.size();


                                    if (retList.size() > 0) {
                                        AdvertiseFilter.this.searchAdapter.clear();
                                        for (JSONObject o : (ArrayList<JSONObject>) retList) {
                                            searchAdapter.add(o);
                                        }


                                        AdvertiseFilter.this.baseActivity.reloadAutoComplete();
                                    }
                                }

                                Constants.hideProgress();
                                AdvertiseFilter.this.onConnection = false;


                            } catch (JSONException e) {
                                try {
                                    AdvertiseFilter.this.onConnection = false;
                                    Constants.hideProgress();
                                }
                                catch (Exception ep){
                                    AdvertiseFilter.this.onConnection = false;
                                }

                            }

                        }


                        public void onFailure(int statusCode,
                                              org.apache.http.Header[] headers,
                                              java.lang.Throwable throwable,
                                              org.json.JSONObject errorResponse) {
                            AdvertiseFilter.this.onConnection = false;
                            Constants.hideProgress();
                        }
                    });
                }
            });
        }



        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        // we clear the adapter and then pupulate it with the new results

    }
}
